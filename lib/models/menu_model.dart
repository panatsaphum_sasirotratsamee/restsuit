class MenuModel {
  String foodname = '', price = '', description = '';

  MenuModel(this.foodname, this.price, this.description);

  MenuModel.fromMap(Map<String, dynamic> map) {
    foodname = map['foodname'];
    price = map['price'];
    description = map['description'];
  }
}
