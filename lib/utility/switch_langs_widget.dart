import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:restsuitapp/utility/app_style.dart';

class SwitchLangs extends StatefulWidget {
  const SwitchLangs({Key? key}) : super(key: key);

  @override
  State<SwitchLangs> createState() => _SwitchLangsState();
}

class _SwitchLangsState extends State<SwitchLangs> {
  bool langs = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 10),
      width: MediaQuery.of(context).size.width / 5,
      child: FlutterSwitch(
        activeText: "   ไทย",
        inactiveText: "Eng",
        activeColor: AppStyle().greenColor,
        inactiveColor: AppStyle().greenColor,
        activeIcon: Image.asset('assets/images/thailand.png'),
        inactiveIcon: Image.asset('assets/images/united-kingdom.png'),
        value: langs,
        valueFontSize: 10.0,
        width: 110,
        borderRadius: 30.0,
        showOnOff: true,

        onToggle: (val) {
          setState(
            () {
              langs = val;
              if (context.locale.languageCode == 'en') {
                context.setLocale(Locale('th', 'TH'));
              } else {
                context.setLocale(Locale('en', 'US'));
              }
            },
          );
        },
      ),
    );
  }
}
