import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/screens/setting/add_menu.dart';
import 'package:restsuitapp/screens/setting/edit_menu.dart';
import 'package:restsuitapp/screens/setting/history.dart';
import 'package:restsuitapp/screens/setting/setting_account.dart';
import 'package:restsuitapp/utility/app_style.dart';

class NavigationDrawerPage extends StatefulWidget {
  String? userPassWord;
  NavigationDrawerPage({Key? key, this.userPassWord}) : super(key: key);

  @override
  _NavigationDrawerPageState createState() => _NavigationDrawerPageState();
}

class _NavigationDrawerPageState extends State<NavigationDrawerPage> {
  String name = '', email = '';
  final padding = const EdgeInsets.symmetric(horizontal: 20);

  @override
  void initState() {
    super.initState();
    findNameAndEmail();
  }

  Future<void> findNameAndEmail() async {
    await Firebase.initializeApp().then((value) async {
      FirebaseAuth.instance.authStateChanges().listen((event) {
        setState(() {
          name = event!.displayName!;
          email = event.email!;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final userName = name;
    final userEmail = email;

    return Drawer(
      child: Material(
        color: AppStyle().yellowDarkColor,
        child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: false,
          children: <Widget>[
            buildHeader(
              userName: userName,
              userEmail: userEmail,
            ),
            Divider(
              color: AppStyle().greenColor,
              endIndent: 20,
              indent: 20,
            ),
            Container(
              padding: padding,
              child: Column(
                children: [
                  buildMenuItem(
                    text: 'drawer.addMenu'.tr(),
                    icon: Icons.post_add,
                    onClicked: () => selectdItem(context, 0),
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  buildMenuItem(
                    text: 'drawer.editMenu'.tr(),
                    icon: Icons.edit_outlined,
                    onClicked: () => selectdItem(context, 1),
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  buildMenuItem(
                    text: 'drawer.history'.tr(),
                    icon: Icons.history_outlined,
                    onClicked: () => selectdItem(context, 2),
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  Divider(
                    color: AppStyle().greenColor,
                  ),
                  buildMenuItem(
                    text: 'drawer.setting'.tr(),
                    icon: Icons.settings,
                    onClicked: () => selectdItem(context, 3),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHeader({
    required String userName,
    required String userEmail,
  }) =>
      InkWell(
        child: Container(
          padding: padding.add(
            const EdgeInsets.symmetric(
              vertical: 30.0,
            ),
          ),
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  width: 70.0,
                  height: 70.0,
                  child: Image.asset('assets/images/logo.png'),
                ),
                const SizedBox(
                  width: 20.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      userName,
                      style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Mitr',
                          color: AppStyle().greenColor),
                    ),
                    Text(
                      userEmail,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Mitr',
                          color: AppStyle().greenColor),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = AppStyle().greenColor;
    return ListTile(
      leading: Icon(
        icon,
        color: color,
      ),
      title: Text(text,
          style: TextStyle(
              color: color,
              fontSize: 18.0,
              fontWeight: FontWeight.normal,
              fontFamily: 'Mitr')),
      onTap: onClicked,
    );
  }

  void selectdItem(BuildContext context, int index) {
    Navigator.of(context).pop();
    switch (index) {
      case 0:
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => AddMenu(),
          ),
        );
        break;
      case 1:
        var editMenu = true;
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => EditMenu(
              editMenu: editMenu,
            ),
          ),
        );
        break;
      case 2:
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => HistoryPage(),
          ),
        );
        break;
      case 3:
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => SettingAccount(
              userPassWord: widget.userPassWord,
            ),
          ),
        );
        break;
    }
  }
}
