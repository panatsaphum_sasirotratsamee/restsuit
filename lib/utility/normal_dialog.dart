import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/utility/app_style.dart';

Future<void> norDialog(BuildContext context, String message) async {
  showDialog(
    context: context,
    builder: (context) => SimpleDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(11.0),
      ),
      title: Center(
          child: Text(
        message,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: AppStyle().redColor,
          fontWeight: FontWeight.normal,
          fontFamily: 'Mitr',
          fontSize: 20.0,
        ),
      )),
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () => Navigator.pop(context),
              child: Text(
                'ok'.tr(),
                style: TextStyle(
                  color: AppStyle().whiteColor,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr',
                  fontSize: 16.0,
                ),
              ),
              style: ElevatedButton.styleFrom(
                primary: Colors.red.shade400,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            ),
          ],
        )
      ],
    ),
  );
}
