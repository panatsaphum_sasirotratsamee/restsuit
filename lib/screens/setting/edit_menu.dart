import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/screens/order_pages/appetizer.dart';
import 'package:restsuitapp/screens/order_pages/dessert.dart';
import 'package:restsuitapp/screens/order_pages/drink.dart';
import 'package:restsuitapp/screens/order_pages/food.dart';
import 'package:restsuitapp/utility/app_style.dart';

class EditMenu extends StatefulWidget {
  String? table;
  bool? editMenu;
  EditMenu({Key? key, this.table, this.editMenu}) : super(key: key);

  @override
  _EditMenuState createState() => _EditMenuState();
}

class _EditMenuState extends State<EditMenu> {
  User? user = FirebaseAuth.instance.currentUser;
  int currenIndex = 0;

  List<Widget> _screens() => [
        AppetizerPage(
          table: widget.table,
          editMenu: widget.editMenu,
        ),
        FoodPage(
          table: widget.table,
          editMenu: widget.editMenu,
        ),
        DessertPage(
          table: widget.table,
          editMenu: widget.editMenu,
        ),
        DrinkPage(
          table: widget.table,
          editMenu: widget.editMenu,
        )
      ];

  @override
  Widget build(BuildContext context) {
    final List<Widget> screens = _screens();
    return Scaffold(
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: const Color(0xffebb327),
        elevation: 0.0,
        title: Text(
          'Edit Menu',
          style: TextStyle(
            fontSize: 35.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
      ),
      body: screens[currenIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index) => setState(() => currenIndex = index),
        selectedItemColor: AppStyle().greenColor,
        selectedFontSize: 12.0,
        unselectedItemColor: AppStyle().yellowDarkColor,
        unselectedFontSize: 10.0,
        unselectedLabelStyle:
            TextStyle(fontWeight: FontWeight.normal, fontFamily: 'Mitr'),
        selectedLabelStyle:
            TextStyle(fontWeight: FontWeight.normal, fontFamily: 'Mitr'),
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        elevation: 10,
        iconSize: 30.0,
        currentIndex: currenIndex,
        items: [
          BottomNavigationBarItem(
            icon: const Icon(Icons.tapas_outlined),
            label: 'Appetizer',
            backgroundColor: AppStyle().whiteColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.ramen_dining_outlined),
            label: 'Food',
            backgroundColor: AppStyle().whiteColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.icecream_outlined),
            label: 'Dessert',
            backgroundColor: AppStyle().whiteColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.local_bar_outlined),
            label: 'Drink',
            backgroundColor: AppStyle().whiteColor,
          )
        ],
      ),
    );
  }
}

Widget showTitltOrder() {
  return Text(
    'Order Menu',
    style: TextStyle(
      fontSize: 48.0,
      color: AppStyle().greenColor,
      fontWeight: FontWeight.bold,
      fontStyle: FontStyle.italic,
      fontFamily: 'Pattaya',
    ),
  );
}
