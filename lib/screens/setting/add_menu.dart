import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/normal_dialog.dart';

class AddMenu extends StatefulWidget {
  bool? editMenu;
  String? category, foodName, price, status, description;
  var docId;
  AddMenu(
      {Key? key,
      this.editMenu,
      this.docId,
      this.category,
      this.foodName,
      this.price,
      this.status,
      this.description})
      : super(key: key);

  @override
  _AddMenuState createState() => _AddMenuState();
}

class _AddMenuState extends State<AddMenu> {
  String foodName = '', description = '', price = '';
  var groupMenu = '', groupStatusMenu = '';
  User? user = FirebaseAuth.instance.currentUser;
  var fieldFoodName = TextEditingController();
  var fieldPrice = TextEditingController();
  var fieldDescription = TextEditingController();

  void clearText() {
    fieldFoodName.clear();
    fieldPrice.clear();
    fieldDescription.clear();
  }

  @override
  void initState() {
    groupStatusMenu = widget.status ?? '';
    fieldFoodName = TextEditingController(text: widget.foodName ?? '');
    fieldPrice = TextEditingController(text: widget.price ?? '');
    fieldDescription = TextEditingController(text: widget.description ?? '');
    foodName = widget.foodName ?? '';
    price = widget.price ?? '';
    description = widget.description ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyle().yellowBG,
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: AppStyle().yellowBG,
        elevation: 0.0,
        title: Text(
          widget.editMenu == true
              ? 'addAndEdit.editMenu'.tr()
              : 'addAndEdit.addNewMenu'.tr(),
          style: TextStyle(
            fontSize: 35.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.topCenter,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Column(
                children: [
                  widget.editMenu != true ? radioAddFood() : Container(),
                  radioStatusFood(),
                  foodNameForm(),
                  priceForm(),
                  descriptionForm(),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  upDateMenuButton(),
                  widget.editMenu == true
                      ? Container(
                          height: MediaQuery.of(context).size.height / 16,
                          width: double.infinity,
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  MediaQuery.of(context).size.width / 10,
                              vertical:
                                  MediaQuery.of(context).size.height / 180),
                          child: ElevatedButton(
                            child: Text(
                              "addAndEdit.deleteMenu".tr(),
                              style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontFamily: 'Mitr',
                                fontSize: 16.0,
                                color: Colors.white,
                              ),
                            ),
                            onPressed: () {
                              popUpMakeSureDeletePage();
                            },
                            style: ElevatedButton.styleFrom(
                              primary: Colors.red.shade400,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32.0),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget radioAddFood() {
    return Theme(
      data: ThemeData(
        unselectedWidgetColor: const Color(0xff006e71),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 12,
            vertical: MediaQuery.of(context).size.height / 80),
        margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 20,
            vertical: MediaQuery.of(context).size.height / 90),
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 10.0,
            ),
            Text(
              'addAndEdit.category'.tr(),
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr',
                  fontSize: 20.0,
                  color: AppStyle().greenColor),
            ),
            Text(
              '*** ' + 'addAndEdit.important'.tr() + ' ***',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr',
                  fontSize: 14.0,
                  color: AppStyle().redColor),
            ),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                          value: 'Appetizer',
                          activeColor: AppStyle().greenColor,
                          groupValue: groupMenu,
                          onChanged: (value) {
                            groupMenu = '';
                            setState(() {
                              groupMenu = value.toString();
                            });
                          },
                        ),
                        Text(
                          'addAndEdit.appetizer'.tr(),
                          style: TextStyle(
                            color: Color(0xff006e71),
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            fontSize: 18.0,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                            value: 'Food',
                            activeColor: AppStyle().greenColor,
                            groupValue: groupMenu,
                            onChanged: (value) {
                              groupMenu = '';
                              setState(() {
                                groupMenu = value.toString();
                              });
                            }),
                        Text(
                          'addAndEdit.food'.tr(),
                          style: TextStyle(
                            color: Color(0xff006e71),
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            fontSize: 18.0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                            value: 'Dessert',
                            activeColor: AppStyle().greenColor,
                            groupValue: groupMenu,
                            onChanged: (value) {
                              groupMenu = '';
                              setState(() {
                                groupMenu = value.toString();
                              });
                            }),
                        Text(
                          'addAndEdit.dessert'.tr(),
                          style: TextStyle(
                            color: Color(0xff006e71),
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            fontSize: 18.0,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Radio(
                            value: 'Drink',
                            activeColor: AppStyle().greenColor,
                            groupValue: groupMenu,
                            onChanged: (value) {
                              groupMenu = '';
                              setState(() {
                                groupMenu = value.toString();
                              });
                            }),
                        Text(
                          'addAndEdit.drink'.tr(),
                          style: TextStyle(
                            color: Color(0xff006e71),
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            fontSize: 18.0,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget radioStatusFood() {
    return Theme(
      data: ThemeData(
        unselectedWidgetColor: const Color(0xff006e71),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 12,
            vertical: MediaQuery.of(context).size.height / 80),
        margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 20,
            vertical: MediaQuery.of(context).size.height / 90),
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 5.0,
            ),
            Text(
              'addAndEdit.status'.tr(),
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr',
                  fontSize: 20.0,
                  color: AppStyle().greenColor),
            ),
            Text(
              '*** ' + 'addAndEdit.important'.tr() + ' ***',
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr',
                  fontSize: 14.0,
                  color: AppStyle().redColor),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Radio(
                    value: 'InStock',
                    activeColor: AppStyle().greenColor,
                    groupValue: groupStatusMenu,
                    onChanged: (value) {
                      setState(() {
                        groupStatusMenu = value.toString();
                      });
                    }),
                Text(
                  'addAndEdit.inStock'.tr(),
                  style: TextStyle(
                    color: AppStyle().greenColor,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr',
                    fontSize: 18.0,
                  ),
                ),
                Theme(
                    data: ThemeData(unselectedWidgetColor: AppStyle().redColor),
                    child: Radio(
                        value: 'OutOfStock',
                        activeColor: AppStyle().redColor,
                        groupValue: groupStatusMenu,
                        onChanged: (value) {
                          setState(() {
                            groupStatusMenu = value.toString();
                          });
                        })),
                Text(
                  'addAndEdit.outStock'.tr(),
                  style: TextStyle(
                    color: AppStyle().redColor,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr',
                    fontSize: 18.0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget upDateMenuButton() {
    return Container(
      height: MediaQuery.of(context).size.height / 16,
      width: double.infinity,
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width / 10,
          vertical: MediaQuery.of(context).size.height / 180),
      child: ElevatedButton(
        child: widget.editMenu == true
            ? Text(
                "addAndEdit.upDateMenu".tr(),
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr',
                  fontSize: 16.0,
                  color: AppStyle().whiteColor,
                ),
              )
            : Text(
                "addAndEdit.addMenu".tr(),
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr',
                  fontSize: 16.0,
                  color: AppStyle().whiteColor,
                ),
              ),
        onPressed: () {
          if (widget.editMenu != true) {
            if (foodName == '' ||
                foodName.isEmpty && price == '' ||
                price.isEmpty) {
              norDialog(context, 'addAndEdit.enterFoodPrice'.tr());
            } else if (price == '' || price.isEmpty) {
              norDialog(context, 'addAndEdit.enterPrice'.tr());
            } else if (foodName == '' || foodName.isEmpty) {
              norDialog(context, 'addAndEdit.enterNameMenu'.tr());
            } else if (groupMenu == '' || groupMenu.isEmpty) {
              norDialog(context, 'addAndEdit.selectCategory'.tr());
            } else if (groupStatusMenu == '' || groupStatusMenu.isEmpty) {
              norDialog(context, 'addAndEdit.selectStatus'.tr());
            } else {
              addNewMenu();
            }
          } else {
            if (foodName == '' ||
                foodName.isEmpty && price == '' ||
                price.isEmpty) {
              norDialog(context, 'addAndEdit.enterPrice'.tr());
            } else if (price == '' || price.isEmpty) {
              norDialog(
                  context, 'addAndEdit.Please enter the food price.'.tr());
            } else if (foodName == '' || foodName.isEmpty) {
              norDialog(context, 'addAndEdit.enterNameMenu'.tr());
            } else if (groupStatusMenu == '' || groupStatusMenu.isEmpty) {
              norDialog(context, 'addAndEdit.selectStatus'.tr());
            } else {
              popUpMakeSureUpdatePage();
            }
          }
        },
        style: ElevatedButton.styleFrom(
          primary: AppStyle().greenColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
        ),
      ),
    );
  }

  Widget foodNameForm() => Container(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 12,
            vertical: MediaQuery.of(context).size.height / 80),
        margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 20,
            vertical: MediaQuery.of(context).size.height / 90),
        child: TextField(
            onChanged: (value) => foodName = value.trim(),
            decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              helperText: '*** ' + 'addAndEdit.enterNameMenu'.tr() + ' ***',
              helperStyle: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr',
                  fontSize: 12.0,
                  color: AppStyle().redColor),
              prefixIcon: Icon(
                Icons.lunch_dining,
                color: Colors.red.shade400,
                size: 35,
              ),
              labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 18.0,
              ),
              labelText: 'addAndEdit.foodName'.tr() + ' :',
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: AppStyle().greenColor)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.green.shade400)),
            ),
            controller: fieldFoodName,
            style: TextStyle(
              fontWeight: FontWeight.normal,
              fontFamily: 'Mitr',
              fontSize: 18.0,
              color: AppStyle().greenColor,
            )),
      );

  Widget descriptionForm() => Container(
        margin: EdgeInsets.symmetric(horizontal: 45, vertical: 2),
        padding: EdgeInsets.all(8),
        child: TextField(
          maxLines: 2,
          onChanged: (value) => description = value.toLowerCase(),
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            labelStyle: TextStyle(
              color: AppStyle().greenColor,
              fontWeight: FontWeight.normal,
              fontFamily: 'Mitr',
              fontSize: 18.0,
            ),
            labelText: 'description'.tr(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
          controller: fieldDescription,
          style: TextStyle(
            fontWeight: FontWeight.normal,
            fontFamily: 'Mitr',
            fontSize: 18.0,
            color: AppStyle().greenColor,
          ),
          keyboardType: TextInputType.text,
        ),
      );

  Widget priceForm() => Container(
        margin: EdgeInsets.symmetric(horizontal: 45, vertical: 2),
        padding: EdgeInsets.all(8),
        child: TextField(
            keyboardType: TextInputType.numberWithOptions(decimal: true),
            onChanged: (value) => price = value.toLowerCase(),
            decoration: InputDecoration(
              suffixText: '฿',
              suffixStyle: const TextStyle(
                color: Colors.red,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 18.0,
              ),
              fillColor: Colors.white,
              filled: true,
              helperText: '*** ' + 'addAndEdit.enterPrice'.tr() + ' ***',
              helperStyle: TextStyle(
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 12.0,
                color: AppStyle().redColor,
              ),
              prefixIcon: Icon(
                Icons.local_offer,
                color: Colors.red.shade400,
                size: 35,
              ),
              labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 18.0,
              ),
              labelText: 'price'.tr() + ' :',
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: AppStyle().greenColor)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.green.shade400)),
            ),
            controller: fieldPrice,
            style: TextStyle(
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 18.0,
                color: AppStyle().greenColor)),
      );

  Future<void> addNewMenu() async {
    Map<String, dynamic> data = {
      'foodname': foodName,
      'price': price,
      'description': description,
      'status': groupStatusMenu
    };
    if (groupMenu == 'Appetizer') {
      await FirebaseFirestore.instance
          .collection('menu')
          .doc(user!.uid)
          .collection('user_appetizer')
          .add(data)
          .then((value) async {
        addSuccessDialog(context, 'addAndEdit.addSuccessful'.tr());
      });
    } else if (groupMenu == 'Food') {
      await FirebaseFirestore.instance
          .collection('menu')
          .doc(user!.uid)
          .collection('user_food')
          .add(data)
          .then((value) async {
        addSuccessDialog(context, 'addAndEdit.addSuccessful'.tr());
      });
    } else if (groupMenu == 'Dessert') {
      await FirebaseFirestore.instance
          .collection('menu')
          .doc(user!.uid)
          .collection('user_dessert')
          .add(data)
          .then((value) async {
        addSuccessDialog(context, 'addAndEdit.addSuccessful'.tr());
      });
    } else if (groupMenu == 'Drink') {
      await FirebaseFirestore.instance
          .collection('menu')
          .doc(user!.uid)
          .collection('user_drink')
          .add(data)
          .then((value) async {
        addSuccessDialog(context, 'addAndEdit.addSuccessful'.tr());
      });
    } else {
      norDialog(context, 'addAndEdit.selectCategory'.tr());
    }
  }

  Future<void> addSuccessDialog(BuildContext context, String message) async {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(11.0),
        ),
        title: Center(
            child: Text(
          message,
          style: TextStyle(
            fontWeight: FontWeight.normal,
            fontFamily: 'Mitr',
            fontSize: 16.0,
          ),
        )),
        children: <Widget>[
          // ignore: deprecated_member_use
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // ignore: deprecated_member_use
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                  clearText();
                  setState(() {
                    groupMenu = '';
                    groupStatusMenu = '';
                  });
                },
                child: Text(
                  'ok'.tr(),
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr',
                    fontSize: 18.0,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red.shade400,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Future<void> updateMenu() async {
    try {
      Map<String, dynamic> data = {
        'status': groupStatusMenu,
        'foodname': foodName,
        'price': price,
        'description': description,
      };
      await FirebaseFirestore.instance
          .collection('menu')
          .doc(user!.uid)
          .collection('${widget.category}')
          .doc('${widget.docId}')
          .update(data)
          .then((value) => {Navigator.pop(context), popUpUploadSuccessPage()});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> deleteMenu() async {
    try {
      await FirebaseFirestore.instance
          .collection('menu')
          .doc(user!.uid)
          .collection('${widget.category}')
          .doc('${widget.docId}')
          .delete()
          .then((value) => {
                Navigator.pop(context),
                popUpDeleteSuccessPage(),
              });
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  void popUpMakeSureUpdatePage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: true,
      isDismissible: true,
      isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              "addAndEdit.sureToUpdate".tr(),
              style: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 18.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  child: ElevatedButton(
                    child: Text(
                      "addAndEdit.update".tr(),
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 16.0,
                        color: AppStyle().whiteColor,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      updateMenu();
                      popUpUploadingPage();
                    },
                    style: ElevatedButton.styleFrom(
                      primary: AppStyle().greenColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  child: ElevatedButton(
                    child: Text(
                      "cancel".tr(),
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 16.0,
                        color: AppStyle().whiteColor,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                      primary: AppStyle().redColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 60,
            ),
          ],
        ),
      ),
    );
  }

  void popUpUploadingPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              'addAndEdit.sendingToCashier'.tr(),
              style: TextStyle(
                color: AppStyle().redColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 18.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                CircularProgressIndicator(
                  strokeWidth: 6,
                  color: AppStyle().redColor,
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void popUpUploadSuccessPage() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
        enableDrag: false,
        isDismissible: false,
        isScrollControlled: true,
        context: context,
        builder: (context) => GestureDetector(
              onTap: () {
                Navigator.pop(context);
                Navigator.pop(context);
              },
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16),
                    ),
                    color: AppStyle().whiteColor),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'transactionSuccess'.tr(),
                      style: TextStyle(
                        color: AppStyle().greenColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 18.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'addAndEdit.allSentToCashier'.tr(),
                      style: TextStyle(
                        color: AppStyle().greenColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 16.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'addAndEdit.tapToMain'.tr(),
                      style: TextStyle(
                        color: AppStyle().greenColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 14.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Column(
                      children: [
                        Icon(
                          Icons.check_circle_outline_rounded,
                          size: 60,
                          color: AppStyle().greenColor,
                        ),
                        SizedBox(
                          height: 50,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ));
  }

  void popUpMakeSureDeletePage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: true,
      isDismissible: true,
      isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              "addAndEdit.sureToDelete".tr(),
              style: TextStyle(
                color: AppStyle().redColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 18.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  child: ElevatedButton(
                    child: Text(
                      "addAndEdit.delete".tr(),
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 14.0,
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                      deleteMenu();
                      popUpDeletingPage();
                    },
                    style: ElevatedButton.styleFrom(
                      primary: AppStyle().redColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  child: ElevatedButton(
                    child: Text(
                      "cancel".tr(),
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 14.0,
                        color: AppStyle().whiteColor,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                      primary: AppStyle().greenColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 60,
            ),
          ],
        ),
      ),
    );
  }

  void popUpDeletingPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      // isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              'addAndEdit.deletingMenu'.tr(),
              style: TextStyle(
                color: AppStyle().redColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr',
                fontSize: 16.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                CircularProgressIndicator(
                  strokeWidth: 6,
                  color: AppStyle().redColor,
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void popUpDeleteSuccessPage() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
        enableDrag: false,
        isDismissible: false,
        // isScrollControlled: true,
        context: context,
        builder: (context) => GestureDetector(
              onTap: () {
                Navigator.pop(context);
                Navigator.pop(context);
              },
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16),
                    ),
                    color: AppStyle().whiteColor),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'transactionSuccess'.tr(),
                      style: TextStyle(
                        color: AppStyle().greenColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 18.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'addAndEdit.menuDeleted'.tr(),
                      style: TextStyle(
                        color: AppStyle().greenColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 16.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'addAndEdit.tapToEdit'.tr(),
                      style: TextStyle(
                        color: AppStyle().greenColor,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        fontSize: 14.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Column(
                      children: [
                        Icon(
                          Icons.check_circle_outline_rounded,
                          size: 60,
                          color: AppStyle().greenColor,
                        ),
                        SizedBox(
                          height: 50,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ));
  }
}
