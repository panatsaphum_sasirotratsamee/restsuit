import 'package:easy_localization/src/public_ext.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/normal_dialog.dart';
import 'package:restsuitapp/utility/switch_langs_widget.dart';

class SettingAccount extends StatefulWidget {
  String? userPassWord;
  SettingAccount({Key? key, this.userPassWord}) : super(key: key);

  @override
  _SettingAccountState createState() => _SettingAccountState();
}

class _SettingAccountState extends State<SettingAccount> {
  String newPassWord = '',
      conPassWord = '',
      oldPassWord = '',
      passWord = '',
      email = '',
      displayName = '';

  @override
  void initState() {
    super.initState();
    findNameAndEmail();
  }

  Future<void> findNameAndEmail() async {
    await Firebase.initializeApp().then((value) async {
      FirebaseAuth.instance.authStateChanges().listen((event) {
        setState(() {
          displayName = event!.displayName!;
          email = event.email!;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyle().yellowBG,
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text(
          'setting.setting'.tr(),
          style: TextStyle(
            fontSize: 30.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
        actions: [SwitchLangs()],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            buildHeader(userName: displayName, userEmail: email),
            SizedBox(
              height: 10,
            ),
            changePasswordForm(),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: const [],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHeader({
    required String userName,
    required String userEmail,
  }) =>
      InkWell(
        child: Container(
          padding: EdgeInsets.all(8),
          margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          decoration: BoxDecoration(
            color: AppStyle().whiteColor,
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: <Widget>[
                      SizedBox(
                        width: 70.0,
                        height: 70.0,
                        child: Image.asset('assets/images/logo.png'),
                      )
                    ],
                  ),
                  const SizedBox(
                    width: 20.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        userName,
                        style: const TextStyle(
                            fontSize: 22.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            color: Colors.black),
                      ),
                      Text(
                        userEmail,
                        style: const TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            color: Colors.black),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: ElevatedButton(
                      onPressed: () {
                        editProfile(context);
                      },
                      child: Text(
                        'setting.edit'.tr(),
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Mitr',
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: AppStyle().greenColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );

  Widget changePasswordForm() => Container(
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
        width: double.infinity,
        decoration: BoxDecoration(
          color: AppStyle().whiteColor,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'setting.changePassword'.tr(),
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        color: AppStyle().greenColor),
                  ),
                ],
              ),
              AppStyle().mySizebox(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 1.3,
                    child: TextField(
                      onChanged: (value) => oldPassWord = value.toLowerCase(),
                      obscureText: true,
                      decoration: InputDecoration(
                        fillColor: Colors.transparent,
                        filled: true,
                        labelStyle: TextStyle(
                            color: AppStyle().greenColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr'),
                        labelText: 'setting.oldPassword'.tr() + ':',
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppStyle().greenColor),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              AppStyle().mySizebox(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 1.3,
                    child: TextField(
                      onChanged: (value) => newPassWord = value.toLowerCase(),
                      obscureText: true,
                      decoration: InputDecoration(
                        fillColor: Colors.transparent,
                        filled: true,
                        labelStyle: TextStyle(
                            color: AppStyle().greenColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr'),
                        labelText: 'setting.newPassword'.tr() + ':',
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: AppStyle().greenColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              AppStyle().mySizebox(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 1.3,
                    child: TextField(
                      onChanged: (value) => conPassWord = value.toLowerCase(),
                      obscureText: true,
                      decoration: InputDecoration(
                        fillColor: Colors.transparent,
                        filled: true,
                        labelStyle: TextStyle(
                            color: AppStyle().greenColor,
                            fontSize: 16.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr'),
                        labelText: 'setting.confirmNewPassword'.tr() + ':',
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: AppStyle().greenColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              AppStyle().mySizebox(),
              Container(
                height: MediaQuery.of(context).size.height / 16,
                width: MediaQuery.of(context).size.width / 1,
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height / 180),
                child: ElevatedButton(
                  onPressed: () {
                    if (oldPassWord == '' ||
                        oldPassWord.isEmpty ||
                        newPassWord == '' ||
                        newPassWord.isEmpty ||
                        conPassWord == '' ||
                        conPassWord.isEmpty) {
                      norDialog(context, 'setting.completeInformation'.tr());
                    } else if (oldPassWord != widget.userPassWord) {
                      norDialog(context, 'setting.oldPasswordsNot'.tr());
                    } else if (newPassWord != conPassWord) {
                      norDialog(context, 'setting.newPasswordsNot'.tr());
                    } else {
                      changePassword(newPassWord);
                    }
                  },
                  child: Text(
                    "confirm".tr(),
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Mitr',
                      color: AppStyle().whiteColor,
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: AppStyle().greenColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32.0),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      );

  Future<void> editProfile(BuildContext context) async {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(11.0),
        ),
        title: Center(
          child: Text(
            'setting.editProfile'.tr(),
            style: TextStyle(
              color: AppStyle().greenColor,
              fontSize: 20.0,
              fontWeight: FontWeight.normal,
              fontFamily: 'Mitr',
            ),
          ),
        ),
        children: [
          Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 12,
                    vertical: MediaQuery.of(context).size.height / 180),
                width: double.infinity,
                child: TextFormField(
                  onChanged: (value) => displayName = value.trim(),
                  initialValue: displayName,
                  decoration: const InputDecoration(
                    fillColor: Colors.transparent,
                    filled: true,
                    labelStyle: TextStyle(
                      color: Colors.black45,
                      fontSize: 18.0,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Mitr',
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.transparent),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () async {
                  await Firebase.initializeApp().then((value) async {
                    FirebaseAuth.instance.authStateChanges().listen((event) {
                      event!.updateDisplayName(displayName).then((value) {
                        findNameAndEmail();
                        Navigator.pop(context);
                      });
                    });
                  });
                },
                child: Text(
                  'setting.save'.tr(),
                  style: TextStyle(
                      color: AppStyle().whiteColor,
                      fontSize: 16.0,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Mitr'),
                ),
                style: ElevatedButton.styleFrom(
                  primary: AppStyle().greenColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  'cancel'.tr(),
                  style: TextStyle(
                      color: AppStyle().whiteColor,
                      fontSize: 16.0,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Mitr'),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red.shade400,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  void changePassword(String password) async {
    User? user = FirebaseAuth.instance.currentUser;
    user!.updatePassword(password).then((_) {
      widget.userPassWord = newPassWord;
      norDialog(context, 'setting.passwordChangedSuccess'.tr());
    }).catchError((e) {
      if (e.code == 'requires-recent-login') {
        norDialog(context, 'setting.Prmissions'.tr());
      } else if (e.code == 'weak-password') {
        norDialog(context, 'setting.passwordMustContain'.tr());
      } else {
        norDialog(context, 'setting.passwordCanNo'.tr() + ' ${e.toString()}');
      }
    });
  }
}
