import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/utility/app_style.dart';

class HistoryDetailPage extends StatefulWidget {
  String? tableHistory;
  String? datetimePayment;
  HistoryDetailPage({Key? key, this.tableHistory, this.datetimePayment})
      : super(
          key: key,
        );

  @override
  _HistoryDetailPageState createState() => _HistoryDetailPageState();
}

class _HistoryDetailPageState extends State<HistoryDetailPage> {
  User? user = FirebaseAuth.instance.currentUser;
  bool showBottom = true, showSum = true;
  int total = 0;
  double sum = 0.00, changeDouble = 0.00, changeSum = 0.00;
  final sumTotal = 0.00;
  String foodName = '',
      descriptionFood = '',
      priceFood1 = '',
      docId1 = '',
      cash = '',
      changeString = '';
  var snapShot, snap, document1111, dateTime;
  List totalList = [];

  @override
  void initState() {
    showBottom = true;
    showSum = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: AppStyle().yellowBG,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: const Color(0xffebb327),
        elevation: 0.0,
        title: Text(
          'history.historyDetail'.tr(),
          style: TextStyle(
            fontSize: 25.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'tableNo'.tr(),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Pattaya',
                    color: AppStyle().greenColor,
                    shadows: const <Shadow>[
                      Shadow(
                        offset: Offset(2.0, 0.0),
                        blurRadius: 3.0,
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                    ],
                  ),
                ),
                Text(
                  '"${widget.tableHistory}"',
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Pattaya',
                    fontWeight: FontWeight.bold,
                    color: AppStyle().redColor,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('history')
            .doc(user!.uid)
            .collection('datetimehistory')
            .doc(widget.datetimePayment)
            .collection('${widget.datetimePayment}')
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                strokeWidth: 6,
                color: AppStyle().whiteColor,
              ),
            );
          }
          return Scrollbar(
            child: ListView(reverse: false, children: [
              Column(
                children: snapshot.data!.docs.map((document) {
                  final price = document['price'];
                  final foodName = document['foodname'];
                  final itemorder = document['itemorder'];
                  final descriptionOrder = document['descriptionorder'];
                  final totalPrice = document['totalprice'];

                  snapShot = snapshot;
                  document1111 = document;
                  docId1 = document.id;
                  snap = snapshot.data!.docs.length;

                  return GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 16),
                      margin: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 8),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8.0)),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 6,
                                  child: Column(
                                    children: [
                                      Text(
                                        foodName,
                                        style: const TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Mitr'),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                          descriptionOrder != ''
                                              ? '*** $descriptionOrder ***'
                                              : descriptionOrder,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: 'Mitr')),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        padding:
                                            EdgeInsets.symmetric(vertical: 8),
                                        margin:
                                            EdgeInsets.symmetric(vertical: 4),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 2,
                                              color:
                                                  AppStyle().yellowLightColor),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: AppStyle().whiteColor,
                                        ),
                                        child: Text(
                                          '${int.parse('$itemorder')} X $price',
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Mitr',
                                          ),
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        padding:
                                            EdgeInsets.symmetric(vertical: 8),
                                        margin:
                                            EdgeInsets.symmetric(vertical: 4),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 2,
                                              color: AppStyle().greenColor),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: AppStyle().whiteColor,
                                        ),
                                        child: Column(
                                          children: [
                                            Text(
                                              '${int.parse('$totalPrice')}' +
                                                  ' ฿',
                                              style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'Mitr',
                                                color: AppStyle().greenColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ),
              getTotal()
            ]),
          );
        },
      ),
    );
  }

  Widget getTotal() {
    sum = 0.00;
    var ds = snapShot.data!.docs;
    for (int i = 0; i < ds.length; i++) {
      sum += (ds[i]['totalprice']).toDouble();
    }
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Text(
        'total'.tr() + ' : $sum',
        style: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.normal,
          fontFamily: 'Mitr',
          color: AppStyle().greenColor,
        ),
      ),
    );
  }
}
