import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/screens/mainscreens/kitchen_detail_page.dart';
import 'package:restsuitapp/utility/app_style.dart';

class KitchenPage extends StatefulWidget {
  const KitchenPage({
    Key? key,
  }) : super(key: key);

  @override
  _KitchenPageState createState() => _KitchenPageState();
}

class _KitchenPageState extends State<KitchenPage> {
  User? user = FirebaseAuth.instance.currentUser;
  var snapShot;
  bool showBottom = true;

  @override
  void initState() {
    showBottom = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: AppStyle().yellowBG,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: const Color(0xffebb327),
        elevation: 0.0,
        title: Text(
          'kitchen.kitchen'.tr(),
          style: TextStyle(
            fontSize: 35.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('kitchen')
            .doc(user!.uid)
            .collection('orderkitchen')
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                strokeWidth: 6,
                color: AppStyle().whiteColor,
              ),
            );
          }
          return GridView.count(
            crossAxisCount: 2,
            reverse: false,
            mainAxisSpacing: 5,
            childAspectRatio: 1.8,
            children: snapshot.data!.docs.map((document) {
              final table = document['table'];
              snapShot = snapshot;
              return GestureDetector(
                child: Container(
                  alignment: Alignment.center,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                  margin:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.0)),
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'tableNo'.tr(),
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr'),
                        ),
                        Text(
                          table,
                          style: TextStyle(
                              color: AppStyle().redColor,
                              fontSize: 24.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr'),
                        ),
                      ],
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => KitchenDetailPage(
                        tableKitchen: table,
                      ),
                    ),
                  );
                },
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
