import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:restsuitapp/screens/mainscreens/cashier.dart';
import 'package:restsuitapp/screens/mainscreens/home.dart';
import 'package:restsuitapp/screens/mainscreens/kitchen.dart';
import 'package:restsuitapp/screens/mainscreens/order.dart';
import 'package:restsuitapp/screens/setting/add_menu.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/navigation_drawer.dart';
import 'package:restsuitapp/utility/normal_dialog.dart';

class Welcome extends StatefulWidget {
  String? userPassWord;
  Welcome({Key? key, this.userPassWord}) : super(key: key);

  @override
  _WelcomeState createState() => _WelcomeState();
}

int currentIndex = 0;
final screens = [
  Welcome(),
  AddMenu(),
];

String table = '';

class _WelcomeState extends State<Welcome> {
  @override
  void initState() {
    table = '';
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: AppStyle().yellowBG,
      drawer: NavigationDrawerPage(
        userPassWord: widget.userPassWord,
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Center(
          child: Image.asset(
            'assets/images/logo.png',
            width: MediaQuery.of(context).size.width / 8,
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        actions: [
          Container(
            width: 30,
            margin: const EdgeInsets.only(right: 20.0),
            child: IconButton(
              onPressed: () {
                logoutDialog(context, 'welcome.wantSignOut'.tr());
              },
              icon: const Icon(
                Icons.power_settings_new,
                size: 30.0,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppStyle().showAppName(),
              AppStyle().mySizebox(),
              orderButton(),
              AppStyle().mySizebox(),
              kitchenButton(),
              AppStyle().mySizebox(),
              cashierButton(),
              AppStyle().mySizebox(),
            ],
          ),
        ),
      ),
    );
  }

  Widget orderButton() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      height: MediaQuery.of(context).size.height / 6.5,
      child: ElevatedButton.icon(
        icon: const Icon(
          Icons.fastfood,
          size: 35.0,
        ),
        label: Text(
          'welcome.order'.tr(),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Mitr',
              fontSize: 26.0,
              color: Color(0xfff08802)),
        ),
        onPressed: () {
          setTable(context);
        },
        style: ElevatedButton.styleFrom(
          primary: AppStyle().greenColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
        ),
      ),
    );
  }

  Widget kitchenButton() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      height: MediaQuery.of(context).size.height / 6.5,
      child: ElevatedButton.icon(
        icon: const Icon(
          Icons.room_service,
          size: 35.0,
        ),
        label: Text(
          'welcome.kitchen'.tr(),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Mitr',
              fontSize: 26.0,
              color: Color(0xfff08802)),
        ),
        onPressed: () {
          MaterialPageRoute materialPageRoute = MaterialPageRoute(
              builder: (BuildContext context) => KitchenPage());
          Navigator.of(context).push(materialPageRoute);
        },
        style: ElevatedButton.styleFrom(
          primary: AppStyle().greenColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
        ),
      ),
    );
  }

  Widget cashierButton() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      height: MediaQuery.of(context).size.height / 6.5,
      child: ElevatedButton.icon(
        icon: const Icon(
          Icons.monetization_on,
          size: 35.0,
        ),
        label: Text(
          'welcome.cashier'.tr(),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Mitr',
              fontSize: 26.0,
              color: Color(0xfff08802)),
        ),
        onPressed: () {
          MaterialPageRoute materialPageRoute = MaterialPageRoute(
              builder: (BuildContext context) => const Cashier());
          Navigator.of(context).push(materialPageRoute);
        },
        style: ElevatedButton.styleFrom(
          primary: AppStyle().greenColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
        ),
      ),
    );
  }

  Future signOut() async {
    try {
      return await FirebaseAuth.instance.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<void> logoutDialog(BuildContext context, String message) async {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        title: Center(
          child: Text(
            message,
            style: TextStyle(
              color: AppStyle().redColor,
              fontWeight: FontWeight.normal,
              fontFamily: 'Mitr',
              fontSize: 20.0,
            ),
          ),
        ),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  signOut();
                  Navigator.of(context).popUntil((route) => route.isFirst);
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const HomeRest()));
                },
                child: Text(
                  "ok".tr(),
                  style: TextStyle(
                    color: AppStyle().whiteColor,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr',
                    fontSize: 16.0,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: AppStyle().greenColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () => {Navigator.pop(context)},
                child: Text(
                  'cancel'.tr(),
                  style: TextStyle(
                    color: AppStyle().whiteColor,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr',
                    fontSize: 16.0,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: AppStyle().pinkColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Future<void> setTable(BuildContext context) async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => SimpleDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(11.0),
        ),
        title: Center(
          child: Text(
            'tableNo',
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
          ).tr(),
        ),
        children: [
          Column(
            children: [
              Container(
              width: 100.0,
                child: TextFormField(
                  onChanged: (value) => table = value.trim(),
                  initialValue: table,
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.zero,
                    fillColor: Colors.transparent,
                    filled: true,
                    labelStyle: TextStyle(
                      color: Colors.black45,
                      fontWeight: FontWeight.normal,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                  ),
                ),
              ),
            ],
          ),
          const Divider(
            color: Colors.transparent,
            height: 20.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                  table = '';
                },
                child: Text(
                  'cancel'.tr(),
                  style: TextStyle(
                    color: AppStyle().whiteColor,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr',
                    fontSize: 20.0,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red.shade400,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  if (table == '' || table.isEmpty) {
                    norDialog(context, 'welcome.enterTableNo'.tr());
                  } else {
                    Navigator.pop(context);
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => OrderMenu(
                          table: table,
                        ),
                      ),
                    );
                  }
                },
                child: Text(
                  'ok'.tr(),
                  style: TextStyle(
                    color: AppStyle().whiteColor,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr',
                    fontSize: 20.0,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: AppStyle().greenColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
