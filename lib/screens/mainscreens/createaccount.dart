import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/models/user_model.dart';
import 'package:restsuitapp/screens/mainscreens/home.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/normal_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';

class CreateAccount extends StatefulWidget {
  String? table;
  CreateAccount({Key? key, @required this.table}) : super(key: key);

  @override
  _CreateAccountState createState() => _CreateAccountState();
}

class _CreateAccountState extends State<CreateAccount> {
  String name = '', user = '', email = '', password = '', confirmpassword = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyle().yellowBG,
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Container(
          alignment: Alignment.center,
          child: SingleChildScrollView(
            child: Column(
              children: [
                showCreateAccount(),
                AppStyle().mySizebox(),
                AppStyle().mySizebox(),
                userNameForm(),
                AppStyle().mySizebox(),
                emailForm(),
                AppStyle().mySizebox(),
                passwordForm(),
                AppStyle().mySizebox(),
                confirmPasswordForm(),
                AppStyle().mySizebox(),
                AppStyle().mySizebox(),
                signupButton(),
                AppStyle().mySizebox(),
              ],
            ),
          )),
    );
  }

  void popUpUploadingPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              'createAccount.creatingAccount'.tr(),
              style: TextStyle(
                  color: AppStyle().redColor,
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr'),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                CircularProgressIndicator(
                  strokeWidth: 6,
                  color: AppStyle().redColor,
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget signupButton() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.2,
      height: MediaQuery.of(context).size.height / 18,
      child: ElevatedButton.icon(
        icon: const Icon(
          Icons.person_add_alt_1,
          size: 20.0,
        ),
        label: Text(
          "createAccount.signUp".tr(),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Mitr',
              fontSize: 20.0,
              color: Colors.white),
        ),
        onPressed: () {
          if (name == '' ||
              name.isEmpty ||
              email == '' ||
              email.isEmpty ||
              password == '' ||
              password.isEmpty ||
              confirmpassword == '' ||
              confirmpassword.isEmpty) {
            norDialog(context, 'createAccount.informationCompletely'.tr());
          } else if (password != confirmpassword) {
            norDialog(context, 'createAccount.passwordsNotMatch'.tr());
          } else {
            registerThread();
            // popUpUploadingPage();
          }
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.red.shade400,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
        ),
      ),
    );
  }

  Future<void> registerThread() async {
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        await value.user!.updateDisplayName(name).then((value2) async {
          String uid = value.user!.uid;
          UserModle model = UserModle(name: name, email: email);
          Map<String, dynamic> data = model.toMap();

          await FirebaseFirestore.instance
              .collection('restname')
              .doc(uid)
              .set(data)
              .then((value) => print('insert success'));
          succeedDialog(context, 'createAccount.userRegistration'.tr());
        });
      });
    } on FirebaseAuthException catch (e) {
      if (e.code == 'email-already-in-use') {
        norDialog(context, 'createAccount.emailAlready'.tr());
      } else if (e.code == 'weak-password') {
        norDialog(context, 'createAccount.passwordMustContain'.tr());
      } else if (e.code == 'invalid-email') {
        norDialog(context, 'createAccount.emailInvalid'.tr());
      } else {
        norDialog(context, '$e'.toString());
        print(e.code);
        print(e.message);
      }
    }
  }

  Widget showCreateAccount() {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width / 14),
      alignment: Alignment.centerLeft,
      child: Text(
        'createAccount.createAccount'.tr(),
        style: TextStyle(
          fontSize: 60.0,
          color: AppStyle().greenColor,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
          fontFamily: 'Pattaya',
          shadows: const <Shadow>[
            Shadow(
              offset: Offset(4.0, 0.0),
              blurRadius: 3.0,
              color: Color.fromARGB(255, 255, 255, 255),
            ),
          ],
        ),
      ),
    );
  }

  Widget userNameForm() => Container(
        width: MediaQuery.of(context).size.width / 1.2,
        child: TextField(
          onChanged: (value) => name = value.toLowerCase(),
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Icon(
              Icons.account_box_outlined,
              color: Colors.red.shade400,
            ),
            labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
            labelText: 'createAccount.userName'.tr(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
        ),
      );

  Widget emailForm() => Container(
        width: MediaQuery.of(context).size.width / 1.2,
        child: TextField(
          keyboardType: TextInputType.emailAddress,
          onChanged: (value) => email = value.toLowerCase(),
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Icon(
              Icons.mail_outline,
              color: Colors.red.shade400,
            ),
            labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
            labelText: 'createAccount.email'.tr(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
        ),
      );

  Widget passwordForm() => Container(
        width: MediaQuery.of(context).size.width / 1.2,
        child: TextField(
          onChanged: (value) => password = value.toLowerCase(),
          obscureText: true,
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Icon(
              Icons.vpn_key_outlined,
              color: Colors.red.shade400,
            ),
            labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
            labelText: 'createAccount.password'.tr(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
        ),
      );

  Widget confirmPasswordForm() => Container(
        width: MediaQuery.of(context).size.width / 1.2,
        child: TextField(
          onChanged: (value) => confirmpassword = value.toLowerCase(),
          obscureText: true,
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Icon(
              Icons.vpn_key_outlined,
              color: Colors.red.shade400,
            ),
            labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
            labelText: 'createAccount.confirmPassword'.tr(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
        ),
      );
}

Future<void> succeedDialog(BuildContext context, String message) async {
  showDialog(
    context: context,
    builder: (context) => SimpleDialog(
      title: Center(
        child: Text(message),
      ),
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            FlatButton(
              onPressed: () {
                MaterialPageRoute materialPageRoute = MaterialPageRoute(
                    builder: (BuildContext context) => const HomeRest());
                Navigator.of(context).push(materialPageRoute);
              },
              child: Text(
                'ok'.tr(),
                style: TextStyle(
                    color: AppStyle().pinkColor,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
              ),
            ),
          ],
        )
      ],
    ),
  );
}
