import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/normal_dialog.dart';
import 'package:intl/intl.dart';

class CashierDetailPage extends StatefulWidget {
  String? tablecashier;
  CashierDetailPage({Key? key, this.tablecashier})
      : super(
          key: key,
        );

  @override
  _CashierDetailPageState createState() => _CashierDetailPageState();
}

class _CashierDetailPageState extends State<CashierDetailPage> {
  User? user = FirebaseAuth.instance.currentUser;
  bool showBottom = true, showSum = true;
  int total = 0;
  double sum = 0.00, changeDouble = 0.00, changeSum = 0.00;
  final sumTotal = 0.00;
  String foodName = '',
      descriptionFood = '',
      priceFood1 = '',
      docId1 = '',
      cash = '',
      changeString = '',
      dateString = '',
      timeString = '';
  var snapShot, snap, document1111, dateTime;
  List totalList = [];

  @override
  void initState() {
    showBottom = true;
    showSum = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: AppStyle().yellowBG,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: const Color(0xffebb327),
        elevation: 0.0,
        title: Text(
          'cashier.cashierDetail'.tr(),
          style: TextStyle(
            fontSize: 25.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'tableNo'.tr() + " : ",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppStyle().greenColor,
                    fontFamily: 'Pattaya',
                    shadows: const <Shadow>[
                      Shadow(
                        offset: Offset(2.0, 0.0),
                        blurRadius: 3.0,
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                    ],
                  ),
                ),
                Text(
                  '"${widget.tablecashier}"',
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Pattaya',
                    color: AppStyle().redColor,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('cashier')
            .doc(user!.uid)
            .collection('datacashier')
            .doc('${widget.tablecashier}')
            .collection('${widget.tablecashier}')
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                strokeWidth: 6,
                color: AppStyle().whiteColor,
              ),
            );
          }
          return Scrollbar(
            child: ListView(
              reverse: false,
              children: [
                Column(
                  children: snapshot.data!.docs.map((document) {
                    final price = document['price'];
                    final foodName = document['foodname'];
                    final itemorder = document['itemorder'];
                    final descriptionOrder = document['descriptionorder'];
                    final totalPrice = document['totalprice'];
                    var docId = document.id;
                    var snap = snapshot.data!.docs.length;
                    snapShot = snapshot;
                    document1111 = document;
                    docId1 = document.id;
                    snap = snapshot.data!.docs.length;

                    Future<void> deleteItemCart() async {
                      try {
                        await FirebaseFirestore.instance
                            .collection('cashier')
                            .doc(user!.uid)
                            .collection('datacashier')
                            .doc('${widget.tablecashier}')
                            .collection('${widget.tablecashier}')
                            .doc(docId)
                            .delete()
                            .then((value) => {print('Delete:$docId')});
                        if (snap == null || snap == 1) {
                          await FirebaseFirestore.instance
                              .collection('cashier')
                              .doc(user!.uid)
                              .collection('datacashier')
                              .doc('${widget.tablecashier}')
                              .delete()
                              .whenComplete(() => print('Field Deleted'));
                        }
                      } on FirebaseException catch (e) {
                        print(e.message);
                      }
                    }

                    Future<void> deleteDialog(
                      BuildContext context,
                    ) async {
                      showDialog(
                        context: context,
                        builder: (context) => SimpleDialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          title: Center(
                            child: Text(
                              "cashier.cancelFoodOrder".tr(),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Mitr'),
                            ),
                          ),
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    deleteItemCart();
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    'ok'.tr(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Mitr'),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    primary: AppStyle().greenColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                ),
                                ElevatedButton(
                                  onPressed: () => {Navigator.pop(context)},
                                  child: Text(
                                    'cancel'.tr(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Mitr'),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    primary: AppStyle().pinkColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      );
                    }

                    return GestureDetector(
                      child: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        margin: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8.0)),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    flex: 6,
                                    child: Column(
                                      children: [
                                        Text(
                                          foodName,
                                          style: const TextStyle(
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: 'Mitr'),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          descriptionOrder != ''
                                              ? '*** $descriptionOrder ***'
                                              : descriptionOrder,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: 'Mitr',
                                              color: AppStyle().redColor),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 4,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          padding:
                                              EdgeInsets.symmetric(vertical: 8),
                                          margin:
                                              EdgeInsets.symmetric(vertical: 4),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: AppStyle().yellowLightColor,
                                          ),
                                          child: Text(
                                            '${int.parse('$itemorder')} X $price',
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: 'Mitr',
                                            ),
                                          ),
                                        ),
                                        Container(
                                          alignment: Alignment.center,
                                          padding:
                                              EdgeInsets.symmetric(vertical: 8),
                                          margin:
                                              EdgeInsets.symmetric(vertical: 4),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: AppStyle().greenColor,
                                          ),
                                          child: Column(
                                            children: [
                                              Text(
                                                '${int.parse('$totalPrice')}' +
                                                    ' ฿',
                                                style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'Mitr',
                                                  color: AppStyle().whiteColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            Icons.delete_outline,
                                            color: AppStyle().redColor,
                                          ),
                                          onPressed: () {
                                            deleteDialog(context);
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                ),
                showSum == true ? getTotal() : Container(),
                showBottom == true
                    ? Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width / 24,
                        ),
                        child: ElevatedButton(
                          child: Text(
                            "cashier.pay".tr(),
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: Colors.white,
                            ),
                          ),
                          onPressed: () {
                            popUpPayMentPage();
                          },
                          style: ElevatedButton.styleFrom(
                            primary: AppStyle().greenColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                          ),
                        ),
                      )
                    : Container()
              ],
            ),
          );
        },
      ),
    );
  }

  Widget getTotal() {
    sum = 0.00;
    var ds = snapShot.data!.docs;

    for (int i = 0; i < ds.length; i++) {
      sum += (ds[i]['totalprice']).toDouble();
    }
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Text(
        'total'.tr() + ' : $sum',
        style: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.normal,
          fontFamily: 'Mitr',
          color: AppStyle().greenColor,
        ),
      ),
    );
  }

  Future<void> ConfirmOrder() async {
    dateTime = DateTime.now();
    final DateFormat dateFormatter = DateFormat('dd-MM-yyyy');
    String dateFormatted = dateFormatter.format(dateTime).toString();
    final DateFormat timeFormatter = DateFormat.Hms();
    String timeFormatted = timeFormatter.format(dateTime).toString();
    dateString = dateFormatted;
    timeString = timeFormatted;
    var snap = snapShot?.data!.docs.length;
    for (int i = 0; i < snap; snap--) {
      if (i <= snap) {
        await saveOrderToHistory();
        await saveToHistory();
        await deleteDocCashier();
        if (snap == null || snap == 1) {
          Navigator.pop(context);
          popUpUploadSuccessPage();
        }
      }
    }
  }

  Future<void> saveToHistory() async {
    try {
      Map<String, dynamic> data = {
        'datetimepayment': dateTime,
        'totalamount': sum,
        'table': widget.tablecashier,
      };
      await FirebaseFirestore.instance
          .collection('history')
          .doc(user!.uid)
          .collection('datetimehistory')
          .doc(dateString + timeString)
          .set(data)
          .then((value) async {});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> saveOrderToHistory() async {
    try {
      Map<String, dynamic> data = {
        'table': document1111['table'],
        'datetime': document1111['datetime'],
        'datetimepayment': dateTime,
        'foodname': document1111['foodname'],
        'price': document1111['price'],
        'descriptionorder': document1111['descriptionorder'],
        'itemorder': document1111['itemorder'],
        'totalprice': document1111['totalprice'],
        'cart': false,
        'kitchen': false,
        'cashier': false,
        'finish': true
      };
      await FirebaseFirestore.instance
          .collection('history')
          .doc(user!.uid)
          .collection('datetimehistory')
          .doc(dateString + timeString)
          .collection(dateString + timeString)
          .add(data)
          .then((value) async {});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> deleteDocCashier() async {
    var snap = snapShot?.data!.docs.length;
    try {
      await FirebaseFirestore.instance
          .collection('cashier')
          .doc(user!.uid)
          .collection('datacashier')
          .doc('${widget.tablecashier}')
          .collection('${widget.tablecashier}')
          .doc(docId1)
          .delete()
          .then((value) => {print('Delete:$docId1')});
      if (snap == null || snap == 1) {
        await FirebaseFirestore.instance
            .collection('cashier')
            .doc(user!.uid)
            .collection('datacashier')
            .doc('${widget.tablecashier}')
            .delete()
            .whenComplete(() => print('Field Deleted'));
      }
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  void popUpUploadingPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              'cashier.saveInHistory'.tr(),
              style: TextStyle(
                  color: AppStyle().redColor,
                  fontSize: 18.0,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr'),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                CircularProgressIndicator(
                  strokeWidth: 6,
                  color: AppStyle().redColor,
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void popUpPayMentPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              color: AppStyle().whiteColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 30,
              ),
              Text(
                'cashier.payment'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 20.0,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 25,
              ),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(16),
                  margin: EdgeInsets.symmetric(vertical: 4, horizontal: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: AppStyle().greenColor,
                  ),
                  child: Row(
                    children: [
                      Text(
                        'total'.tr() + ' : ',
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            color: AppStyle().whiteColor),
                      ),
                      Expanded(
                        child: Text(
                          '$sum',
                          style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: AppStyle().whiteColor),
                          textAlign: TextAlign.right,
                        ),
                      )
                    ],
                  )),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(16),
                  margin: EdgeInsets.symmetric(vertical: 4, horizontal: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: AppStyle().yellowDarkColor,
                  ),
                  child: Row(
                    children: [
                      Text(
                        'cashier.cash'.tr() + ' : ',
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            color: AppStyle().greenColor),
                      ),
                      Expanded(child: cashForm())
                    ],
                  )),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: ElevatedButton(
                          child: Text(
                            "back".tr(),
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: AppStyle().whiteColor,
                            ),
                          ),
                          onPressed: () {
                            setState(() {
                              cash = '';
                            });
                            Navigator.pop(context);
                          },
                          style: ElevatedButton.styleFrom(
                            side: BorderSide(
                                width: 2, color: AppStyle().redColor),
                            primary: AppStyle().redColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(16),
                        child: ElevatedButton(
                          child: Text(
                            "cashier.calculate".tr(),
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: AppStyle().whiteColor,
                            ),
                          ),
                          onPressed: () {
                            if (cash == '' || cash.isEmpty) {
                              norDialog(context, 'cashier.enterCash'.tr());
                            } else {
                              changeDouble = double?.parse(cash);

                              changeSum = changeDouble - sum;

                              changeString = changeSum.toString();

                              if (changeDouble < sum) {
                                norDialog(
                                    context,
                                    '*** ' +
                                        'cashier.enoughCash'.tr() +
                                        ' ***\n' +
                                        'cashier.missing'.tr() +
                                        '\n "${changeSum.abs()}"');
                              } else if (changeSum == 0.0000) {
                                Navigator.pop(context);
                                popUpUploadingPage();
                                setState(() {
                                  showSum = false;
                                  showBottom = false;
                                });
                                ConfirmOrder();
                              } else {
                                Navigator.pop(context);
                                popUpChangePage(changeSum);
                              }
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            side: BorderSide(
                                width: 2, color: AppStyle().greenColor),
                            primary: AppStyle().greenColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void popUpChangePage(change) {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              color: AppStyle().whiteColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 30,
              ),
              Text(
                'cashier.payment'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 25,
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(25),
                margin: EdgeInsets.symmetric(vertical: 4, horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppStyle().yellowLightColor,
                ),
                child: Row(
                  children: [
                    Text(
                      'cashier.change'.tr() + ' : ',
                      style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Mitr',
                          color: AppStyle().greenColor),
                    ),
                    Expanded(
                      child: Text(
                        '$changeString',
                        style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            color: AppStyle().greenColor),
                        textAlign: TextAlign.right,
                      ),
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.all(16),
                        child: ElevatedButton(
                          child: Text(
                            "back".tr(),
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: AppStyle().whiteColor,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);

                            setState(() {
                              cash = '';
                            });
                            popUpPayMentPage();
                          },
                          style: ElevatedButton.styleFrom(
                            side: BorderSide(
                                width: 2, color: AppStyle().redColor),
                            primary: AppStyle().redColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(16),
                        child: ElevatedButton(
                          child: Text(
                            "done".tr(),
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: AppStyle().whiteColor,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            popUpUploadingPage();
                            setState(() {
                              showSum = false;
                              showBottom = false;
                            });
                            ConfirmOrder();
                          },
                          style: ElevatedButton.styleFrom(
                            side: BorderSide(
                                width: 2, color: AppStyle().greenColor),
                            primary: AppStyle().greenColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void popUpUploadSuccessPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => GestureDetector(
        child: Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              color: AppStyle().whiteColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 30,
              ),
              Text(
                'transactionSuccess'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                '*** ' + 'cashier.storedInHistory'.tr() + ' ***',
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 16.0,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'cashier.tapToCashier'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 14.0,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 25,
              ),
              Column(
                children: [
                  Icon(
                    Icons.check_circle_outline_rounded,
                    size: 60,
                    color: AppStyle().greenColor,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ],
          ),
        ),
        onTap: () {
          Navigator.pop(context);
          Navigator.pop(context);
        },
      ),
    );
  }

  Widget cashForm() => Container(
        child: TextField(
          onChanged: (value) => cash = value.toLowerCase(),
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            labelStyle: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
          style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.normal,
              fontFamily: 'Mitr',
              color: AppStyle().greenColor),
          keyboardType: TextInputType.numberWithOptions(decimal: true),
        ),
      );
}
