import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/screens/cart/cart.dart';
import 'package:restsuitapp/screens/mainscreens/welcome.dart';
import 'package:restsuitapp/screens/order_pages/appetizer.dart';
import 'package:restsuitapp/screens/order_pages/dessert.dart';
import 'package:restsuitapp/screens/order_pages/drink.dart';
import 'package:restsuitapp/screens/order_pages/food.dart';
import 'package:restsuitapp/utility/app_style.dart';

class OrderMenu extends StatefulWidget {
  String? table;
  OrderMenu({Key? key, @required this.table}) : super(key: key);

  @override
  _OrderMenuState createState() => _OrderMenuState();
}

class _OrderMenuState extends State<OrderMenu> {
  User? user = FirebaseAuth.instance.currentUser;
  int currenIndex = 0;

  List<Widget> _screens() => [
        AppetizerPage(
          table: widget.table,
        ),
        FoodPage(
          table: widget.table,
        ),
        DessertPage(
          table: widget.table,
        ),
        DrinkPage(
          table: widget.table,
        )
      ];

  @override
  Widget build(BuildContext context) {
    final List<Widget> screens = _screens();
    return Scaffold(
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: const Color(0xffebb327),
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => Welcome()),
                (route) => false);
          },
        ),
        title: Text(
          'order.orderMenu'.tr(),
          style: TextStyle(
            fontSize: 35.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
        actions: [
          Container(
            width: 45,
            margin: const EdgeInsets.only(right: 15.0),
            child: IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CartPage(table: widget.table),
                  ),
                );
              },
              icon: Icon(
                Icons.shopping_cart_outlined,
                size: 35.0,
                color: AppStyle().greenColor,
              ),
            ),
          ),
        ],
      ),
      body: screens[currenIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index) => setState(() => currenIndex = index),
        selectedItemColor: AppStyle().greenColor,
        selectedFontSize: 12.0,
        unselectedItemColor: AppStyle().yellowDarkColor,
        unselectedFontSize: 12.0,
        unselectedLabelStyle:
            TextStyle(fontWeight: FontWeight.normal, fontFamily: 'Mitr'),
        selectedLabelStyle:
            TextStyle(fontWeight: FontWeight.normal, fontFamily: 'Mitr'),
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        iconSize: 30.0,
        currentIndex: currenIndex,
        elevation: 10,
        items: [
          BottomNavigationBarItem(
            icon: const Icon(Icons.tapas_outlined),
            label: 'order.appetizer'.tr(),
            backgroundColor: AppStyle().whiteColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.ramen_dining_outlined),
            label: 'order.food'.tr(),
            backgroundColor: AppStyle().whiteColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.icecream_outlined),
            label: 'order.dessert'.tr(),
            backgroundColor: AppStyle().whiteColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.local_bar_outlined),
            label: 'order.drink'.tr(),
            backgroundColor: AppStyle().whiteColor,
          )
        ],
      ),
    );
  }
}
