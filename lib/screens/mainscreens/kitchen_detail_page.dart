import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/utility/app_style.dart';

class KitchenDetailPage extends StatefulWidget {
  String? tableKitchen;
  KitchenDetailPage({Key? key, this.tableKitchen})
      : super(
          key: key,
        );

  @override
  _KitchenDetailPageState createState() => _KitchenDetailPageState();
}

class _KitchenDetailPageState extends State<KitchenDetailPage> {
  User? user = FirebaseAuth.instance.currentUser;
  bool showBottom = true;
  String foodName = '', descriptionFood = '', priceFood1 = '', docId1 = '';
  var snapShot, document1111;

  @override
  void initState() {
    showBottom = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: AppStyle().yellowBG,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: const Color(0xffebb327),
        elevation: 0.0,
        title: Text(
          'kitchen.kitchenDetail'.tr(),
          style: TextStyle(
            fontSize: 35.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('kitchen')
            .doc(user!.uid)
            .collection('orderkitchen')
            .doc('${widget.tableKitchen}')
            .collection('${widget.tableKitchen}')
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                strokeWidth: 6,
                color: AppStyle().whiteColor,
              ),
            );
          }

          return Scrollbar(
            child: ListView(reverse: false, children: [
              Column(
                children: snapshot.data!.docs.map((document) {
                  final table = document['table'];
                  final foodName = document['foodname'];
                  final itemorder = document['itemorder'];
                  final descriptionOrder = document['descriptionorder'];
                  var kitchen = document['kitchen'];
                  var cashier = document['cashier'];
                  var docId = document.id;
                  var snap = snapshot.data!.docs.length;
                  snapShot = snapshot;
                  document1111 = document;
                  docId1 = document.id;

                  Future<void> updateItemCart(
                      kitchenStatus, cashierStatus) async {
                    try {
                      Map<String, dynamic> data = {
                        'kitchen': kitchenStatus,
                        'cashier': cashierStatus,
                      };
                      await FirebaseFirestore.instance
                          .collection('kitchen')
                          .doc(user!.uid)
                          .collection('orderkitchen')
                          .doc('${widget.tableKitchen}')
                          .collection('${widget.tableKitchen}')
                          .doc(docId)
                          .update(data)
                          .then((value) =>
                              {print('kitchenStatus&cashierStatus is update')});
                    } on FirebaseException catch (e) {
                      print(e.message);
                    }
                  }

                  Future<void> deleteItemCart() async {
                    try {
                      await FirebaseFirestore.instance
                          .collection('kitchen')
                          .doc(user!.uid)
                          .collection('orderkitchen')
                          .doc('${widget.tableKitchen}')
                          .collection('${widget.tableKitchen}')
                          .doc(docId)
                          .delete()
                          .then((value) => {print('Delete:$docId')});
                      if (snap == null || snap == 1) {
                        await FirebaseFirestore.instance
                            .collection('kitchen')
                            .doc(user!.uid)
                            .collection('orderkitchen')
                            .doc('${widget.tableKitchen}')
                            .delete()
                            .whenComplete(() => print('Field Deleted'));
                      }
                    } on FirebaseException catch (e) {
                      print(e.message);
                    }
                  }

                  Future<void> deleteDialog(
                    BuildContext context,
                  ) async {
                    showDialog(
                      context: context,
                      builder: (context) => SimpleDialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        title: Center(
                          child: Text(
                            "kitchen.cancelOrder".tr(),
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 18.0,
                                fontWeight: FontWeight.normal,
                                fontFamily: 'Mitr'),
                          ),
                        ),
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  deleteItemCart();
                                  Navigator.pop(context);
                                },
                                child: Text(
                                  'ok'.tr(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'Mitr'),
                                ),
                                style: ElevatedButton.styleFrom(
                                  primary: AppStyle().greenColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () => {Navigator.pop(context)},
                                child: Text(
                                  'cancel'.tr(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'Mitr'),
                                ),
                                style: ElevatedButton.styleFrom(
                                  primary: AppStyle().pinkColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    );
                  }

                  return GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 16),
                      margin: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 8),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8.0)),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: Column(
                                    children: [
                                      Text(
                                        foodName,
                                        style: const TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Mitr'),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        descriptionOrder != ''
                                            ? '*** $descriptionOrder ***'
                                            : descriptionOrder,
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Mitr',
                                            color: AppStyle().redColor),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 8),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 8),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: AppStyle().yellowLightColor,
                                    ),
                                    child: Text(
                                      '${int.parse('$itemorder')}',
                                      style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Mitr',
                                      ),
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black),
                                        children: [
                                          TextSpan(
                                            text: 'tableNo'.tr(),
                                            style: TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: 'Mitr'),
                                          ),
                                          TextSpan(
                                            text: ' ${table}',
                                            style: TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: 'Mitr',
                                              color: AppStyle().redColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    kitchen == true
                                        ? Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 12, vertical: 8),
                                            child: ElevatedButton.icon(
                                              icon: Icon(
                                                Icons.fastfood_outlined,
                                                color: AppStyle().greenColor,
                                              ),
                                              label: Text(
                                                'kitchen.cook'.tr(),
                                                // "done".tr(),
                                                style: TextStyle(
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: 'Mitr',
                                                  color: AppStyle().greenColor,
                                                ),
                                              ),
                                              onPressed: () {
                                                kitchen = false;
                                                cashier = true;
                                                updateItemCart(
                                                    kitchen, cashier);
                                              },
                                              style: ElevatedButton.styleFrom(
                                                side: BorderSide(
                                                    width: 2,
                                                    color:
                                                        AppStyle().greenColor),
                                                primary: AppStyle().whiteColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          32.0),
                                                ),
                                              ),
                                            ),
                                          )
                                        : Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 12, vertical: 8),
                                            child: ElevatedButton.icon(
                                              icon: Icon(
                                                Icons.check_circle,
                                                color: AppStyle().whiteColor,
                                              ),
                                              label: Text(
                                                "done".tr(),
                                                style: TextStyle(
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: 'Mitr',
                                                  color: AppStyle().whiteColor,
                                                ),
                                              ),
                                              onPressed: () {
                                                kitchen = true;
                                                cashier = false;
                                                updateItemCart(
                                                    kitchen, cashier);
                                              },
                                              style: ElevatedButton.styleFrom(
                                                primary: AppStyle().greenColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          32.0),
                                                ),
                                              ),
                                            ),
                                          ),
                                    GestureDetector(
                                      onTap: () {
                                        deleteDialog(context);
                                      },
                                      child: Icon(
                                        Icons.delete_outline,
                                        color: AppStyle().redColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ),
              snapShot?.data!.docs.length == null && showBottom == true
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              SizedBox(
                                height: 30,
                              ),
                              Text(
                                'cart.noOrder'.tr(),
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Mitr',
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  : showBottom == true
                      ? Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                          margin: EdgeInsets.all(8),
                          child: ElevatedButton(
                            child: Text(
                              "kitchen.allDone".tr(),
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.normal,
                                fontFamily: 'Mitr',
                                color: Colors.white,
                              ),
                            ),
                            onPressed: () {
                              popUpMakeSurePage();
                            },
                            style: ElevatedButton.styleFrom(
                              primary: Colors.red.shade400,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32.0),
                              ),
                            ),
                          ),
                        )
                      : Container()
            ]),
          );
        },
      ),
    );
  }

  Future<void> ConfirmOrder() async {
    var snap = snapShot?.data!.docs.length;
    for (int i = 0; i < snap; snap--) {
      if (i <= snap) {
        await confirmOderToCashier();
        await confirmTableToCashier();
        await deleteDocOrder();
        if (snap == null || snap == 1) {
          Navigator.pop(context);
          popUpUploadSuccessPage();
        }
      }
    }
  }

  Future<void> confirmTableToCashier() async {
    try {
      Map<String, dynamic> data = {
        'table': document1111['table'],
      };
      await FirebaseFirestore.instance
          .collection('cashier')
          .doc(user!.uid)
          .collection('datacashier')
          .doc('${widget.tableKitchen}')
          .set(data)
          .then((value) async {});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> confirmOderToCashier() async {
    try {
      Map<String, dynamic> data = {
        'table': document1111['table'],
        'datetime': document1111['datetime'],
        'foodname': document1111['foodname'],
        'price': document1111['price'],
        'descriptionorder': document1111['descriptionorder'],
        'itemorder': document1111['itemorder'],
        'totalprice': document1111['totalprice'],
        'cart': false,
        'kitchen': false,
        'cashier': true,
        'finish': false
      };
      await FirebaseFirestore.instance
          .collection('cashier')
          .doc(user!.uid)
          .collection('datacashier')
          .doc('${widget.tableKitchen}')
          .collection('${widget.tableKitchen}')
          .add(data)
          .then((value) async {});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> deleteDocOrder() async {
    var snap = snapShot?.data!.docs.length;
    try {
      await FirebaseFirestore.instance
          .collection('kitchen')
          .doc(user!.uid)
          .collection('orderkitchen')
          .doc('${widget.tableKitchen}')
          .collection('${widget.tableKitchen}')
          .doc(docId1)
          .delete()
          .then((value) => {print('Delete:$docId1')});
      if (snap == null || snap == 1) {
        await FirebaseFirestore.instance
            .collection('kitchen')
            .doc(user!.uid)
            .collection('orderkitchen')
            .doc('${widget.tableKitchen}')
            .delete()
            .whenComplete(() => print('Field Deleted'));
      }
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  void popUpMakeSurePage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: true,
      isDismissible: true,
      isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              "kitchen.sendToCashier".tr(),
              style: TextStyle(
                  color: AppStyle().redColor,
                  fontSize: 20.0,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr'),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "*** " + 'kitchen.doneEverything'.tr() + " ***",
              style: TextStyle(
                  color: AppStyle().redColor,
                  fontSize: 14.0,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr'),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  child: ElevatedButton(
                    child: Text(
                      "kitchen.allDone".tr(),
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        showBottom = false;
                      });
                      Navigator.pop(context);
                      ConfirmOrder();
                      popUpUploadingPage();
                    },
                    style: ElevatedButton.styleFrom(
                      primary: AppStyle().greenColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  child: ElevatedButton(
                    child: Text(
                      "cancel".tr(),
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Mitr',
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                      primary: AppStyle().redColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }

  void popUpUploadingPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              'kitchen.sendingToCashier'.tr(),
              style: TextStyle(
                  color: AppStyle().redColor,
                  fontSize: 20.0,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr'),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 25,
            ),
            Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                CircularProgressIndicator(
                  strokeWidth: 6,
                  color: AppStyle().redColor,
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void popUpUploadSuccessPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => GestureDetector(
        onTap: () {
          Navigator.pop(context);
          Navigator.pop(context);
        },
        child: Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              color: AppStyle().whiteColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 30,
              ),
              Text(
                'transactionSuccess'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 20.0,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'kitchen.sentCashier'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 16.0,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'kitchen.tapToKitchen'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 12.0,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 25,
              ),
              SizedBox(
                height: 25,
              ),
              Column(
                children: [
                  Icon(
                    Icons.check_circle_outline_rounded,
                    size: 60,
                    color: AppStyle().greenColor,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
