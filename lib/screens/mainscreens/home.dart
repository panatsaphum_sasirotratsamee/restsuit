import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/screens/mainscreens/createaccount.dart';
import 'package:restsuitapp/screens/mainscreens/signin.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/switch_langs_widget.dart';

class HomeRest extends StatefulWidget {
  const HomeRest({Key? key}) : super(key: key);

  @override
  _HomeRestState createState() => _HomeRestState();
}

class _HomeRestState extends State<HomeRest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyle().yellowBG,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        actions: const [SwitchLangs()],
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/egg.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              AppStyle().showLogo(),
              AppStyle().mySizebox25(),
              AppStyle().showAppName(),
              AppStyle().mySizebox(),
              AppStyle().mySizebox(),
              signInButton(),
              AppStyle().mySizebox(),
              signUpButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget signInButton() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.2,
      height: MediaQuery.of(context).size.height / 14,
      child: ElevatedButton.icon(
        icon: const Icon(
          Icons.account_circle_rounded,
          size: 20.0,
        ),
        label: Text(
          'home.signin'.tr(),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Mitr',
              fontSize: 20.0,
              color: Colors.white),
        ),
        onPressed: () {
          MaterialPageRoute materialPageRoute = MaterialPageRoute(
              builder: (BuildContext context) => const SignIn());
          Navigator.of(context).push(materialPageRoute);
        },
        style: ElevatedButton.styleFrom(
          primary: const Color(0xfff08802),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
        ),
      ),
    );
  }

  Widget signUpButton() {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 1.2,
      height: MediaQuery.of(context).size.height / 14,
      child: ElevatedButton.icon(
        icon: const Icon(
          Icons.person_add_alt_1_rounded,
          size: 20.0,
        ),
        label: Text(
          'home.signup'.tr(),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Mitr',
              fontSize: 20.0,
              color: Color(0xfff08802)),
        ),
        onPressed: () {
          MaterialPageRoute materialPageRoute = MaterialPageRoute(
              builder: (BuildContext context) => CreateAccount());
          Navigator.of(context).push(materialPageRoute);
        },
        style: ElevatedButton.styleFrom(
          primary: const Color(0xff006e71),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
        ),
      ),
    );
  }
}
