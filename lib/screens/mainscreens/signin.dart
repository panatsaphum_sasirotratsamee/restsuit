import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/screens/mainscreens/welcome.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/normal_dialog.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  String user = '', email = '', password = '';
  final FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyle().yellowBG,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Container(
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              showCreateAccount(),
              AppStyle().mySizebox(),
              emailForm(),
              AppStyle().mySizebox(),
              passwordForm(),
              AppStyle().mySizebox(),
              signInButton(),
              AppStyle().mySizebox(),
            ],
          ),
        ),
      ),
    );
  }

  Widget signInButton() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.2,
      height: MediaQuery.of(context).size.height / 18,
      child: ElevatedButton.icon(
        icon: const Icon(
          Icons.account_circle_rounded,
          size: 20.0,
        ),
        label: Text(
          "signIn.signIn".tr(),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: 'Mitr',
              fontSize: 20.0,
              color: AppStyle().whiteColor),
        ),
        onPressed: () {
          // email='test@gmail.com';
          // password='123456';
          if (email == '' &&
              email.isEmpty &&
              password == '' &&
              password.isEmpty) {
            norDialog(context, 'signIn.enterEmailPassword'.tr());
          } else if (email == '' || email.isEmpty) {
            norDialog(context, 'signIn.enterEmail'.tr());
          } else if (password == '' || password.isEmpty) {
            norDialog(context, 'signIn.enterPassword'.tr());
          } else {
            registerThread();
          }
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.red.shade400,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
        ),
      ),
    );
  }

  Future<void> registerThread() async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      Navigator.of(context).popUntil((route) => route.isFirst);
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Welcome(
                    userPassWord: password,
                  )));
    } on FirebaseAuthException catch (e) {
      if (e.code == 'wrong-password') {
        norDialog(context, 'signIn.incorrectPassword'.tr());
      } else if (e.code == 'user-not-found') {
        norDialog(context, 'signIn.emailNotRegistered'.tr());
      } else {
        norDialog(
            context,
            'signIn.somethingWrong'.tr() +
                'signIn.error'.tr() +
                '${e.message}');
        print(e.code);
        print(e.message);
      }
    }
  }

  Widget showCreateAccount() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width / 14),
      child: Text(
        'signIn.signAccount'.tr(),
        style: TextStyle(
          fontSize: 60.0,
          color: AppStyle().greenColor,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
          fontFamily: 'Pattaya',
          shadows: const <Shadow>[
            Shadow(
              offset: Offset(4.0, 0.0),
              blurRadius: 3.0,
              color: Color.fromARGB(255, 255, 255, 255),
            ),
          ],
        ),
      ),
    );
  }

  Widget emailForm() => Container(
        width: MediaQuery.of(context).size.width / 1.2,
        child: TextFormField(
          keyboardType: TextInputType.emailAddress,
          onChanged: (value) => email = value.toLowerCase(),
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Icon(
              Icons.mail_outline,
              color: Colors.red.shade400,
            ),
            labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
            labelText: 'signIn.email'.tr(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
        ),
      );

  Widget passwordForm() => Container(
        width: MediaQuery.of(context).size.width / 1.2,
        child: TextFormField(
          onChanged: (value) => password = value.toLowerCase(),
          obscureText: true,
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            prefixIcon: Icon(
              Icons.vpn_key_outlined,
              color: Colors.red.shade400,
            ),
            labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
            labelText: 'signIn.password'.tr(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
        ),
      );

  Future<void> gotoSignIn(BuildContext context, String message) async {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        title: Text(message),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FlatButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'ok'.tr(),
                  style: TextStyle(
                      color: AppStyle().pinkColor,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Mitr'),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
