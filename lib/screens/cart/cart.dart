import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:restsuitapp/screens/mainscreens/welcome.dart';
import 'package:restsuitapp/utility/app_style.dart';

class CartPage extends StatefulWidget {
  String? table;
  CartPage({Key? key, @required this.table}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  User? user = FirebaseAuth.instance.currentUser;
  int itemOrder1 = 0, itemOrderr = 0, totalPrice1 = 0, totalPricee = 0;
  bool showBottom = true;
  String foodName1 = '', descriptionFood1 = '', priceFood1 = '', docId1 = '';
  var document1111, snapshot1111, snapShot;
  var dateTimeOrder;
  var finishStatus, cartStatus, kitchenStatus, cashierStatus;
  final fieldDescription = TextEditingController();

  @override
  void initState() {
    showBottom = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: AppStyle().yellowBG,
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: AppStyle().redColor),
        backgroundColor: const Color(0xffebb327),
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_outlined),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          'cart.cart'.tr(),
          style: TextStyle(
            fontSize: 35.0,
            color: AppStyle().greenColor,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            fontFamily: 'Pattaya',
            shadows: const <Shadow>[
              Shadow(
                offset: Offset(4.0, 0.0),
                blurRadius: 3.0,
                color: Color.fromARGB(255, 255, 255, 255),
              ),
            ],
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'tableNo'.tr(),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Pattaya',
                    color: AppStyle().greenColor,
                    shadows: const <Shadow>[
                      Shadow(
                        offset: Offset(2.0, 0.0),
                        blurRadius: 3.0,
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                    ],
                  ),
                ),
                Text(
                  '"${widget.table}"',
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Pattaya',
                    color: AppStyle().redColor,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('cart')
            .doc(user!.uid)
            .collection('ordercart')
            .doc('${widget.table}')
            .collection('${widget.table}')
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                strokeWidth: 6,
                color: AppStyle().whiteColor,
              ),
            );
          }
          return Container(
            child: ListView(
              children: [
                Column(
                  children: snapshot.data!.docs.map((document) {
                    final foodName = document['foodname'];
                    final descriptionOrder = document['descriptionorder'];
                    final priceFood = document['price'];
                    final dateTime = document['datetime'] ?? '';
                    final finish = document['finish'] ?? '';
                    final cart = document['cart'] ?? '';
                    final kitchen = document['kitchen'] ?? '';
                    final cashier = document['cashier'] ?? '';
                    int totalPrice = document['totalprice'];
                    int itemOrder = document['itemorder'];
                    var docId = document.id;
                    var priceFoodd = int.parse(priceFood);

                    docId1 = docId;
                    foodName1 = foodName;
                    priceFood1 = priceFood;
                    itemOrder1 = itemOrder;
                    descriptionFood1 = descriptionOrder ?? "";
                    totalPrice1 = totalPrice;
                    document1111 = document;
                    snapShot = snapshot;

                    snapshot1111 = snapshot.data!.docs.length;
                    dateTimeOrder = dateTime;
                    finishStatus = finish;
                    cartStatus = cart;
                    kitchenStatus = kitchen;
                    cashierStatus = cashier;

                    Future<void> updateItemCart(cartStatus) async {
                      try {
                        Map<String, dynamic> data = {
                          'totalprice': totalPricee,
                          'itemorder': itemOrderr,
                          'kitchen': kitchenStatus,
                          'cart': cartStatus
                        };
                        await FirebaseFirestore.instance
                            .collection('cart')
                            .doc(user!.uid)
                            .collection('ordercart')
                            .doc('${widget.table}')
                            .collection('${widget.table}')
                            .doc(docId)
                            .update(data)
                            .then((value) => {print('UpdateItem:$itemOrderr')});
                      } on FirebaseException catch (e) {
                        print(e.message);
                      }
                    }

                    Future<void> deleteItemCart() async {
                      try {
                        await FirebaseFirestore.instance
                            .collection('cart')
                            .doc(user!.uid)
                            .collection('ordercart')
                            .doc('${widget.table}')
                            .collection('${widget.table}')
                            .doc(docId)
                            .delete()
                            .then((value) => {print('Delete:$itemOrderr')});
                      } on FirebaseException catch (e) {
                        print(e.message);
                      }
                    }

                    Future<void> deleteDialog(
                      BuildContext context,
                    ) async {
                      showDialog(
                        context: context,
                        builder: (context) => SimpleDialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          title: Center(
                            child: Text(
                              "cart.wouldRemove".tr(),
                              style: TextStyle(
                                  color: AppStyle().redColor,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Mitr'),
                            ),
                          ),
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ElevatedButton(
                                  onPressed: () {
                                    deleteItemCart();
                                    print(
                                        'snapLang:${snapShot?.data!.docs.length}');
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    'ok'.tr(),
                                    style: TextStyle(
                                        color: AppStyle().whiteColor,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Mitr'),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    primary: AppStyle().greenColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                ),
                                ElevatedButton(
                                  onPressed: () => {Navigator.pop(context)},
                                  child: Text(
                                    'cancel'.tr(),
                                    style: TextStyle(
                                        color: AppStyle().whiteColor,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Mitr'),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    primary: AppStyle().pinkColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      );
                    }

                    return SingleChildScrollView(
                      // controller: _controller,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white,
                        ),
                        padding: EdgeInsets.all(8),
                        margin:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 5,
                              child: Column(
                                children: [
                                  Text(
                                    foodName,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Mitr'),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    descriptionFood1 != ''
                                        ? '*** $descriptionFood1 ***'
                                        : descriptionFood1,
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'Mitr',
                                        color: AppStyle().redColor),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Column(
                                children: [
                                  IconButton(
                                    onPressed: () {
                                      setState(() {
                                        if (document['foodname'] == foodName &&
                                            document['price'] == priceFood)
                                          itemOrderr = itemOrder;
                                        itemOrderr++;
                                        totalPricee = itemOrderr * priceFoodd;
                                        cartStatus == true;
                                        updateItemCart(cartStatus);
                                      });
                                    },
                                    icon: Icon(
                                      Icons.add,
                                      color: AppStyle().greenColor,
                                    ),
                                    iconSize: 30,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 8),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 8),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.transparent,
                                    ),
                                    child: Text(
                                      '${int.parse('$itemOrder')}',
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: 'Mitr',
                                          color: AppStyle().greenColor),
                                    ),
                                  ),
                                  IconButton(
                                    onPressed: () {
                                      if (itemOrder >= 2 &&
                                          document['foodname'] == foodName &&
                                          document['price'] == priceFood) {
                                        setState(() {
                                          itemOrderr = itemOrder;
                                          itemOrderr--;
                                          totalPricee = itemOrderr * priceFoodd;
                                          cartStatus == true;
                                          updateItemCart(cartStatus);
                                        });
                                      }
                                    },
                                    icon: Icon(
                                      Icons.remove,
                                      size: 30,
                                      color: AppStyle().greenColor,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Column(
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        padding:
                                            EdgeInsets.symmetric(vertical: 8),
                                        margin:
                                            EdgeInsets.symmetric(vertical: 4),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: AppStyle().greenColor,
                                        ),
                                        child: Column(
                                          children: [
                                            Text(
                                              '${int.parse('$totalPrice')}' +
                                                  ' ฿',
                                              style: TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'Mitr',
                                                color: AppStyle().whiteColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      IconButton(
                                        icon: Icon(
                                          Icons.delete_outline,
                                          color: AppStyle().redColor,
                                        ),
                                        onPressed: () {
                                          deleteDialog(context);
                                        },
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }).toList(),
                ),
                snapShot?.data!.docs.length == null && showBottom == true
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Text(
                                    'cart.noOrder'.tr(),
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Mitr',
                                      color: AppStyle().whiteColor,
                                    ),
                                  ),
                                ],
                              ))
                        ],
                      )
                    : showBottom == true
                        ? Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 16),
                            margin: EdgeInsets.all(8),
                            child: ElevatedButton(
                              child: Text(
                                "confirm".tr(),
                                style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Mitr',
                                  color: AppStyle().whiteColor,
                                ),
                              ),
                              onPressed: () {
                                popUpUploadMenuPage();
                                setState(() {
                                  showBottom = false;
                                  cartStatus = false;
                                  kitchenStatus = true;
                                  ConfirmOrder();
                                });
                              },
                              style: ElevatedButton.styleFrom(
                                primary: Colors.red.shade400,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(32.0),
                                ),
                              ),
                            ),
                          )
                        : Container()
              ],
            ),
          );
        },
      ),
    );
  }

  Future<void> ConfirmOrder() async {
    var snap = snapShot?.data!.docs.length;
    for (int i = 0; i < snap; snap--) {
      if (i <= snap) {
        await confirmOderToKitchen();
        await confirmTableToKitchen();
        await deleteDocOrder();
        // Navigator.pop(context);
        if (snap == null || snap == 1) {
          deleteItemCart();
          Navigator.pop(context);
          popUpUploadSuccessMenuPage();
        }
      }
    }
  }

  Future<void> confirmOderToKitchen() async {
    try {
      Map<String, dynamic> data = {
        'table': document1111['table'],
        'datetime': document1111['datetime'],
        'foodname': document1111['foodname'],
        'price': document1111['price'],
        'descriptionorder': document1111['descriptionorder'],
        'itemorder': document1111['itemorder'],
        'totalprice': document1111['totalprice'],
        'cart': false,
        'kitchen': true,
        'cashier': false,
        'finish': false
      };
      await FirebaseFirestore.instance
          .collection('kitchen')
          .doc(user!.uid)
          .collection('orderkitchen')
          .doc('${widget.table}')
          .collection('${widget.table}')
          .add(data)
          .then((value) {});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> confirmTableToKitchen() async {
    try {
      Map<String, dynamic> data = {
        'table': document1111['table'],
      };
      await FirebaseFirestore.instance
          .collection('kitchen')
          .doc(user!.uid)
          .collection('orderkitchen')
          .doc('${widget.table}')
          .set(data)
          .then((value) async {});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> deleteDocOrder() async {
    try {
      await FirebaseFirestore.instance
          .collection('cart')
          .doc(user!.uid)
          .collection('ordercart')
          .doc('${widget.table}')
          .collection('${widget.table}')
          .doc(docId1)
          .delete()
          .then((value) => {print('Delete:$docId1')});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> deleteItemCart() async {
    try {
      await FirebaseFirestore.instance
          .collection('cart')
          .doc(user!.uid)
          .collection('ordercart')
          .doc('${widget.table}')
          .delete()
          .whenComplete(() => {
                print('DeleteTable:${widget.table}'),
              });
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  void popUpUploadMenuPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: false,
      isDismissible: false,
      context: context,
      builder: (context) => Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            color: AppStyle().whiteColor),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              'cart.sendingToKitchen'.tr(),
              style: TextStyle(
                  color: AppStyle().redColor,
                  fontSize: 20.0,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Mitr'),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 50,
            ),
            CircularProgressIndicator(
              strokeWidth: 6,
              color: AppStyle().redColor,
            ),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

  void popUpUploadSuccessMenuPage() {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: true,
      isDismissible: false,
      isScrollControlled: true,
      context: context,
      builder: (context) => GestureDetector(
        onTap: () {
          Navigator.of(context).popUntil((route) => route.isFirst);
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Welcome()));
        },
        child: Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              color: AppStyle().whiteColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 30,
              ),
              Text(
                'transactionSuccess'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 26.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'cart.allSentKitchen'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'cart.tapToMain'.tr(),
                style: TextStyle(
                    color: AppStyle().greenColor,
                    fontSize: 12.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Mitr'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 25,
              ),
              Column(
                children: [
                  Icon(
                    Icons.check_circle_outline_rounded,
                    size: 60,
                    color: AppStyle().greenColor,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
