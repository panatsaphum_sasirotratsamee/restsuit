import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/utility/app_style.dart';

class DetailMenu extends StatefulWidget {
  String? table;
  String? foodCollection;
  final foodName;
  final descriptionFood;
  final priceFood;
  DetailMenu(
      {Key? key,
      @required this.table,
      @required this.foodCollection,
      this.foodName,
      this.descriptionFood,
      this.priceFood})
      : super(key: key);

  @override
  _DetailMenuState createState() => _DetailMenuState();
}

class _DetailMenuState extends State<DetailMenu> {
  final fieldDescription = TextEditingController();
  String descriptionOrder = '';
  var totalPrice = 0;
  int itemOrder = 1;
  User? user = FirebaseAuth.instance.currentUser;
  int itemOrder1 = 0, itemOrderr = 0, totalPrice1 = 0, totalPricee = 0;
  String foodName1 = '', descriptionFood1 = '', priceFood1 = '', docId1 = '';
  var document1111, snapshot1111;
  var dateTimeOrder;
  var finishStatus, cartStatus, kitchenStatus, cashierStatus;

  @override
  Widget build(BuildContext context) {
    var pricefood = int.parse(widget.priceFood);
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('cart')
          .doc(user!.uid)
          .collection('${widget.table}')
          .snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(
              strokeWidth: 6,
              color: AppStyle().whiteColor,
            ),
          );
        }
        snapshot.data!.docs.map((document) {
          final foodName = document['foodname'] ?? '';
          final descriptionOrder = document['descriptionorder'] ?? '';
          final priceFood = document['price'] ?? '';
          final dateTime = document['datetime'] ?? '';
          final finish = document['finish'] ?? '';
          final cart = document['cart'] ?? '';
          final kitchen = document['kitchen'] ?? '';
          final cashier = document['cashier'] ?? '';
          int totalPrice = document['totalprice'] ?? '';
          int itemOrder = document['itemorder'] ?? '';
          var docId = document.id;

          docId1 = docId;
          foodName1 = foodName;
          priceFood1 = priceFood;
          itemOrder1 = itemOrder;
          descriptionFood1 = descriptionOrder;
          totalPrice1 = totalPrice;
          document1111 = document;
          snapshot1111 = snapshot.data!.docs.length;
          dateTimeOrder = dateTime;
          finishStatus = finish;
          cartStatus = cart;
          kitchenStatus = kitchen;
          cashierStatus = cashier;
        });
        return Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16),
                ),
                color: AppStyle().yellowLightColor),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: SizedBox(),
                      ),
                      Expanded(
                        flex: 5,
                        child: Text(
                          'detailMenu.detailsMenu'.tr(),
                          style: TextStyle(
                            fontSize: 35.0,
                            color: AppStyle().greenColor,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            fontFamily: 'Pattaya',
                            shadows: const <Shadow>[
                              Shadow(
                                offset: Offset(4.0, 0.0),
                                blurRadius: 3.0,
                                color: Color.fromARGB(255, 255, 255, 255),
                              ),
                            ],
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Icon(
                              Icons.clear,
                              size: 30,
                            )),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: AppStyle().whiteColor),
                    child: Column(
                      children: [
                        Text(
                          '${widget.foodName}',
                          style: const TextStyle(
                              fontSize: 24.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr'),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          widget.descriptionFood == ''
                              ? ''
                              : '" ${widget.descriptionFood} "',
                          style: const TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: Colors.grey),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          'price'.tr() + ' ${widget.priceFood} ฿',
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: AppStyle().redColor),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          itemOrder == 1
                              ? 'total'.tr() + ' $pricefood ฿'
                              : 'total'.tr() + ' $totalPrice ฿',
                          style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.normal,
                              fontFamily: 'Mitr',
                              color: AppStyle().redColor),
                        )
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'detailMenu.amount'.tr(),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr'),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Container(
                          height: 45,
                          width: 100,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: AppStyle().whiteColor),
                          child: Center(
                            child: Text(
                              '$itemOrder',
                              style: TextStyle(
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Mitr',
                                  color: AppStyle().greenColor),
                            ),
                          )),
                      const SizedBox(
                        width: 10,
                      ),
                      Column(
                        children: [
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  itemOrder++;
                                  totalPrice = itemOrder * pricefood;
                                });
                              },
                              icon: Icon(
                                Icons.add,
                                color: AppStyle().greenColor,
                              )),
                          IconButton(
                              onPressed: () {
                                if (itemOrder >= 2) {
                                  setState(() {
                                    itemOrder--;
                                    totalPrice = itemOrder * pricefood;
                                  });
                                }
                              },
                              icon: Icon(
                                Icons.remove,
                                color: AppStyle().greenColor,
                              ))
                        ],
                      ),
                    ],
                  ),
                  descriptionForm(),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 40.0,
                    width: 300.0,
                    child: ElevatedButton(
                      onPressed: () {
                        var dateTime = DateTime.now();
                        addToCart(dateTime);
                        confirmTableToCart();
                        docId1.isEmpty
                            ? addSuccessDialog(
                                context, 'detailMenu.addMenuSuccess'.tr())
                            : updateCart(dateTime);
                      },
                      child: Text(
                        'detailMenu.addToOrder'.tr(),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr'),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red.shade400,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(13.0),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ));
      },
    );
  }

  descriptionForm() => Container(
        padding: EdgeInsets.all(8),
        child: TextFormField(
          keyboardType: TextInputType.text,
          maxLines: 3,
          onChanged: (value) => descriptionOrder = value.toLowerCase(),
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            labelStyle: TextStyle(
                color: AppStyle().greenColor,
                fontWeight: FontWeight.normal,
                fontFamily: 'Mitr'),
            labelText: 'description'.tr(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppStyle().greenColor)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green.shade400)),
          ),
          controller: fieldDescription,
        ),
      );

  Future<void> addToCart(dateTime) async {
    var pricefood = int.parse(widget.priceFood);
    try {
      Map<String, dynamic> data = {
        'table': widget.table,
        'datetime': dateTime,
        'foodname': widget.foodName,
        'price': widget.priceFood,
        'descriptionorder': descriptionOrder,
        'itemorder': itemOrder,
        'totalprice': itemOrder == 1 ? pricefood : totalPrice,
        'cart': true,
        'kitchen': false,
        'cashier': false,
        'finish': false
      };
      await FirebaseFirestore.instance
          .collection('cart')
          .doc(user!.uid)
          .collection('ordercart')
          .doc('${widget.table}')
          .collection('${widget.table}')
          .add(data)
          .then((value) async {
        print('Add new menu is successful.');
      });
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> confirmTableToCart() async {
    try {
      Map<String, dynamic> data = {
        'table': widget.table,
      };
      await FirebaseFirestore.instance
          .collection('cart')
          .doc(user!.uid)
          .collection('ordercart')
          .doc('${widget.table}')
          .set(data)
          .then((value) async {});
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> updateCart(dateTime) async {
    var pricefood = int.parse(widget.priceFood);
    try {
      Map<String, dynamic> data = {
        'table': widget.table,
        'datetime': dateTime,
        'foodname': widget.foodName,
        'price': widget.priceFood,
        'descriptionorder': descriptionOrder,
        'itemorder': itemOrder,
        'totalprice': itemOrder == 1 ? pricefood : totalPrice,
        'cart': true,
        'kitchen': false,
        'cashier': false,
        'finish': false
      };
      await FirebaseFirestore.instance
          .collection('cart')
          .doc(user!.uid)
          .collection('ordercart')
          .doc('${widget.table}')
          .collection('${widget.table}')
          .doc(docId1)
          .update(data)
          .then((value) async {
        print('Add new menu is successful.');
      });
    } on FirebaseException catch (e) {
      print(e.message);
    }
  }

  Future<void> addSuccessDialog(BuildContext context, String message) async {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(11.0),
        ),
        title: Center(
            child: Text(
          message,
          style: TextStyle(
              color: AppStyle().greenColor,
              fontSize: 20.0,
              fontWeight: FontWeight.normal,
              fontFamily: 'Mitr'),
        )),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: Text(
                  'ok'.tr(),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Mitr'),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red.shade400,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
