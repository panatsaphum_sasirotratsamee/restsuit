import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/screens/order_pages/detailmenu.dart';
import 'package:restsuitapp/screens/setting/add_menu.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/normal_dialog.dart';

class AppetizerPage extends StatefulWidget {
  String? table;
  int? totalPriceAppetizer;
  bool? editMenu;
  AppetizerPage({Key? key, @required this.table, this.editMenu})
      : super(key: key);

  @override
  _AppetizerPageState createState() => _AppetizerPageState();
}

class _AppetizerPageState extends State<AppetizerPage> {
  User? user = FirebaseAuth.instance.currentUser;
  final ScrollController _controller = ScrollController();
  String appetizer = 'user_appetizer';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      backgroundColor: AppStyle().yellowBG,
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('menu')
            .doc(user!.uid)
            .collection('user_appetizer')
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(
                strokeWidth: 6,
                color: AppStyle().whiteColor,
              ),
            );
          }
          return Scrollbar(
            child: ListView(
              children: snapshot.data!.docs.map((document) {
                final foodName = document['foodname'];
                final descriptionFood = document['description'];
                final priceFood = document['price'];
                final status = document['status'];
                var docId = document.id;
                return SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                    margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0)),
                    child: Container(
                      child: ListTile(
                        title: Text(
                          foodName,
                          style: const TextStyle(
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            fontSize: 18.0,
                          ),
                        ),
                        subtitle: Text(
                          descriptionFood,
                          style: const TextStyle(
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Mitr',
                            fontSize: 16.0,
                          ),
                        ),
                        trailing: status == 'InStock' ? Text(
                          priceFood + ' ฿',
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Mitr',
                              color: AppStyle().redColor),
                        ):Text(
                          'depleted'.tr(),
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Mitr',
                              color: AppStyle().redColor),
                        ),
                        onTap: () {
                          widget.editMenu == true
                              ? Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => AddMenu(
                                      editMenu: widget.editMenu,
                                      category: appetizer,
                                      docId: docId,
                                      foodName: foodName,
                                      price: priceFood,
                                      description: descriptionFood,
                                      status: status,
                                    ),
                                  ),
                                )
                              : status == 'InStock'
                                  ? popUpDetailMenuPage(
                                      foodName, descriptionFood, priceFood)
                                  : norDialog(
                                      context, '* $foodName '+'isDepleted'.tr() +' *');
                        },
                      ),
                    ),
                  ),
                );
              }).toList(),
            ),
          );
        },
      ),
    );
  }

  void popUpDetailMenuPage(foodName, descriptionFood, priceFood) {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      enableDrag: true,
      isDismissible: true,
      isScrollControlled: true,
      context: context,
      builder: (context) => Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: DetailMenu(
            table: widget.table,
            foodName: foodName,
            descriptionFood: descriptionFood,
            priceFood: priceFood,
            foodCollection: appetizer),
      ),
    );
  }
}
