import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:restsuitapp/screens/setting/add_menu.dart';
import 'package:restsuitapp/utility/app_style.dart';
import 'package:restsuitapp/utility/normal_dialog.dart';
import 'detailmenu.dart';

class DessertPage extends StatefulWidget {
  String? table;
  bool? editMenu;
  DessertPage({Key? key, @required this.table, this.editMenu})
      : super(key: key);

  @override
  _DessertPageState createState() => _DessertPageState();
}

class _DessertPageState extends State<DessertPage> {
  User? user = FirebaseAuth.instance.currentUser;
  int itemOrder = 0;
  String dessert = 'user_dessert';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyle().yellowBG,
      extendBodyBehindAppBar: false,
      body:StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('menu')
              .doc(user!.uid)
              .collection('user_dessert')
              .snapshots(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(
                  strokeWidth: 6,
                  color: AppStyle().whiteColor,
                ),
              );
            }
            return Scrollbar(child: ListView(
              children: snapshot.data!.docs.map((document) {
                final foodName = document['foodname'];
                final descriptionFood = document['description'];
                final priceFood = document['price'];
                final status = document['status'];
                var docId = document.id;
                print(document.toString());
                return SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                    margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0)),
                    child: Container(
                      child: ListTile(
                        title: Text(
                          foodName,
                          style: const TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.normal,fontFamily: 'Mitr'),
                        ),
                        subtitle: Text(
                          descriptionFood,
                          style: const TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.normal,fontFamily: 'Mitr'),
                        ),
                        trailing: status == 'InStock' ? Text(
                          priceFood + ' ฿',
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Mitr',
                              color: AppStyle().redColor),
                        ):Text(
                          'depleted'.tr(),
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Mitr',
                              color: AppStyle().redColor),
                        ),
                        onTap: () {
                          widget.editMenu == true
                              ? Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AddMenu(
                                editMenu: widget.editMenu,
                                category: dessert,
                                docId: docId,
                                foodName: foodName,
                                price: priceFood,
                                description: descriptionFood,
                                status: status,
                              ),
                            ),
                          )
                              : document['status'] == 'InStock'
                              ?
                          popUpDetailMenuPage(
                              foodName, descriptionFood, priceFood)
                              : norDialog(
                              context, '* $foodName '+'isDepleted'.tr() +' *');
                        },
                      ),
                    ),
                  ),
                );
              }).toList(),
            ),);
          },
        ),
    );
  }

  void popUpDetailMenuPage(foodName, descriptionFood, priceFood) {
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
        enableDrag: true,
        isDismissible: true,
        isScrollControlled: true,
        context: context,
        builder: (context) => Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: DetailMenu(
                  table: widget.table,
                  foodName: foodName,
                  descriptionFood: descriptionFood,
                  priceFood: priceFood,
                  foodCollection: dessert),
            ),);
  }
}

